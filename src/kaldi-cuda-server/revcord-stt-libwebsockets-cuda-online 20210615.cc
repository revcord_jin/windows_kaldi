// cudadecoderbin/batched-wav-nnet3-cuda-online.cc
//
// Copyright (c) 2019, NVIDIA CORPORATION.  All rights reserved.
// Hugo Braun
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//#define lws_verbose
#include <queue>

//#define MEMORY_LEAKS_DETECTION

#ifdef MEMORY_LEAKS_DETECTION
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#if HAVE_CUDA == 1

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <nvToolsExt.h>
#include <iomanip>
#include "cudadecoder/batched-threaded-nnet3-cuda-online-pipeline.h"
#include "cudadecoder/cuda-online-pipeline-dynamic-batcher.h"
#include "cudadecoderbin/cuda-bin-tools.h"
//#include "cudadecoderbin/custom_backend.h"
#include "cudadecoderbin/cuda-speaker-diarization-tools.h"
#include "cudamatrix/cu-allocator.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "nnet3/am-nnet-simple.h"
#include "nnet3/nnet-utils.h"
#include "util/kaldi-thread.h"
#include "lat/sausages.h"
#include "json.h"

#include <iostream>
#include <istream>
#include <streambuf>
#include <string>


//Web socket-related includes
#define LWS_DLL
#define LWS_INTERNAL
#include "libwebsockets.h"
#include <signal.h>
#pragma comment(lib, "websockets.lib") // comment out when ubuntu
#pragma comment(lib, "libprotobuf.lib") // comment out when ubuntu
//#pragma comment(lib, "libprotobufd.lib") // comment out when ubuntu

#include "wavchunk.pb.h"
using namespace revcord;
using namespace std;


enum ErrorCodes {
    kSuccess,
    kUnknown,
    kInvalidModelConfig,
    kGpuNotSupported,
    kSequenceBatcher,
    kModelControl,
    kInputOutput,
    kInputName,
    kOutputName,
    kInputOutputDataType,
    kInputContents,
    kInputSize,
    kOutputBuffer,
    kBatchTooBig,
    kTimesteps,
    kChunkTooBig
};

using namespace std;
using namespace kaldi;
using namespace cuda_decoder;

CudaOnlineBinaryOptions opts_;
BatchedThreadedNnet3CudaOnlinePipeline* cuda_pipeline_ = NULL;
CudaOnlinePipelineDynamicBatcher* dynamic_batcher_ = NULL;

fst::SymbolTable* word_syms_;
BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID correlation_id_ = 0;

bool latest_sent_msg_was_buffered = false;
// Reply with ....
unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 65536 * 8 + LWS_SEND_BUFFER_POST_PADDING];
unsigned char* writable_buffer = &buf[LWS_SEND_BUFFER_PRE_PADDING];


std::mutex writer_m_;
size_t writable_size_ = 0;

//Result STT relevant connection
struct lws* sttWSI;


// Contants 
const uint64_t int32_byte_size_ = 4;
const uint64_t int64_byte_size_ = 8;
const uint64_t chunk_num_bytes_ = 8160;
const uint64_t chunk_num_samps_ = 4080;// 8000 * 0.51

#ifdef XVECTOR_SD
Vector<BaseFloat> xvector_mean_;
Matrix<BaseFloat> xvector_transform_;
Plda plda_;
BatchedXvectorComputer* xvector_computer_;

std::string SpeakerDiarization(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat,
    const Matrix<BaseFloat>& h_utt_features,
    const BatchedXvectorComputer& xvector_computer,
    const Vector<BaseFloat>& xvector_mean,
    const Matrix<BaseFloat>& xvector_transform,
    const Plda& plda) {

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    for (int i = 0; i < size; i++) {
        json::JSON word;
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }

    std::vector<Vector<BaseFloat>> xvectors = xvector_computation(xvector_computer, h_utt_features, word_sequence);
    bool has_xvector = (xvectors.size() > 0);
    std::vector<int32> spk_ids;
    int num_speakers = VBDiarization(opts.batched_xvector_opts, xvector_mean, xvector_transform, plda, xvectors, spk_ids);
    std::string out_str = MakeFinalJSON(has_xvector, spk_ids, num_speakers, word_sequence);
    return out_str;
}
#endif

std::string BuildFinalResult(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat) 
{

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    json::JSON word;
    for (int i = 0; i < size; i++) {
        
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }
    std::string out_str = MakeFinalJSONOnlyForSTT(word_sequence);
    return out_str;
}

//*****************************
//*** AUDIO STREAMING LAYER ***
//*****************************

//Server configuration definitions
#define TIME_INTERVAL 50
const int DEFAULT_SERVER_PORT = 2571;
static int interrupted, port = DEFAULT_SERVER_PORT;

// audio stream buffers
const int MAX_NUM_CHANNELS = 1024;

struct audio_streaming_protocol_instance {
    char buffer[ chunk_num_bytes_ + sizeof(WavChunk)];
    BaseFloat wav[chunk_num_samps_];
    uint64_t total_bytes_received;
    unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 65536 * 8 + LWS_SEND_BUFFER_POST_PADDING];
    size_t writable_size;
    bool live_stt;
    int n_total_chunks;
};



bool WriteSTTChunk(struct lws* wsi, STTChunk chunk, struct audio_streaming_protocol_instance* ldi)
{
    std::lock_guard<std::mutex> lk(writer_m_);
    ldi->writable_size = chunk.ByteSizeLong();
    unsigned char* writable_buffer = &ldi->buf[LWS_SEND_BUFFER_PRE_PADDING];
    chunk.SerializeToArray(writable_buffer, ldi->writable_size);
    return true;
}
bool EmptyReply(struct lws* wsi, struct audio_streaming_protocol_instance* ldi) {
    std::string output = "{\"text\": \"\"}";
    STTChunk chunk;
    chunk.set_corr_id(-1);
    chunk.set_is_partial_chunk(false);
    chunk.set_is_result_chunk(false);
    chunk.set_is_final_chunk(false);
    chunk.set_result(output);
    return WriteSTTChunk(wsi, chunk, ldi);
}
bool ReplyIfAny(struct lws* wsi, struct audio_streaming_protocol_instance* ldi) {
    /*
    if (ldi->writable_size == 0) {// empty reply
        std::string output = "{\"text\": \"\"}";
        STTChunk chunk;
        chunk.set_corr_id(-1);
        chunk.set_is_partial_chunk(false);
        chunk.set_is_result_chunk(false);
        chunk.set_is_final_chunk(false);
        chunk.set_result(output);
        WriteSTTChunk(wsi, chunk, ldi);
    }
    */
    lws_callback_on_writable(wsi);
    return true;
}

int Execute(struct lws* wsi, WavChunk chunk, struct audio_streaming_protocol_instance* ldi) {

    if (Info_Kind::Info_Kind_REFRESH == chunk.info().kind())
    {
        cuda_pipeline_->FreeChannelMap();
        correlation_id_ = 0;
        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << std::endl << "Initialized the server " << std::ctime(&end_time) << std::endl;
        ReplyIfAny(wsi, ldi);
        return kSuccess;
    }
    if (Info_Kind::Info_Kind_DUMMY == chunk.info().kind())
    {
        dynamic_batcher_->WaitForCompletion();
        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << std::endl << "Waited for the completion " << std::ctime(&end_time) << std::endl;
        ReplyIfAny(wsi, ldi);
        return kSuccess;
    }
    if (Info_Kind::Info_Kind_LIVESTT == chunk.info().kind())
    {
        ldi->live_stt = true;
        ReplyIfAny(wsi, ldi);
        return kSuccess;
    }
    if (Info_Kind::Info_Kind_CORRID == chunk.info().kind())
    {
        std::string output = "{\"text\": \"\"}";
        STTChunk chunk;
        chunk.set_corr_id(correlation_id_ ++);
        chunk.set_is_partial_chunk(false);
        chunk.set_is_result_chunk(false);
        chunk.set_is_final_chunk(false);
        chunk.set_result(output);
        ldi->n_total_chunks = 0;
        WriteSTTChunk(wsi, chunk, ldi);
        ReplyIfAny(wsi, ldi);
        return kSuccess;
    }
    else if (Info_Kind::Info_Kind_GETSTT == chunk.info().kind())
    {
        /* final result 
        *  Client should send a request with Info_Kind_GETSTT to get a result 
        */
        ReplyIfAny(wsi, ldi); // reply the final result ready after last chunk arrived.
        return kSuccess;
    }
    else if (Info_Kind::Info_Kind_WAV == chunk.info().kind())
    {

        int32_t length = chunk.samples().length() / 2;
        short* wav_short = (short*)(chunk.samples().data());
        for (int i = 0; i < length; i++)
            ldi->wav[i] = (BaseFloat)wav_short[i];
        SubVector<BaseFloat> wave_part(ldi->wav, length);

        bool start = chunk.is_first_chunk();
        bool end = chunk.is_last_chunk();
        BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID corr_id = chunk.corr_id();
        if (start) {
            // Defining the callback for the partial results
            cuda_pipeline_->SetBestPathCallback(
                corr_id,
                [corr_id, wsi, ldi](
                    const std::string& str, bool partial, bool endpoint_detected) {

                        STTChunk chunk;
                        json::JSON res;
                        if (endpoint_detected)
                        {
                            res["text"] = str;
                            chunk.set_corr_id(corr_id);
                            chunk.set_is_partial_chunk(false);
                            chunk.set_is_result_chunk(true);
                            chunk.set_is_final_chunk(false);
                            //chunk.set_result(res.dump());
                            chunk.set_result(str);
                        }
                        else if (partial) {
                            res["partial"] = str;
                            chunk.set_corr_id(corr_id);
                            chunk.set_is_partial_chunk(true);
                            chunk.set_is_result_chunk(false);
                            chunk.set_is_final_chunk(false);
                            //chunk.set_result(res.dump());
                            chunk.set_result(str);
                        }
                        WriteSTTChunk(wsi, chunk, ldi); //partial result
                        //cout << "corr_id=" << corr_id << "# " << res.dump() << endl;

                });
        }
        if (end) {
            // If last chunk, set the callback for that seq
            cuda_pipeline_->SetLatticeCallback(
#ifdef XVECTOR_SD
                corr_id, [corr_id](CompactLattice& clat, Matrix<BaseFloat>& h_utt_features) {
                    std::string output = SpeakerDiarization(opts_, *word_syms_, clat, h_utt_features, *xvector_computer_, xvector_mean_, xvector_transform_, plda_);
                    //std::string output = "{\"text\": \"\"}";
#else
                corr_id, [corr_id, wsi, ldi](CompactLattice& clat) {
                    std::string output = "{\"text\": \"\"}";
                    //std::string output = BuildFinalResult(opts_, *word_syms_, clat);   
		            //std::cout << "corr_id#" << corr_id << std::endl << output << std::endl;
#endif

                    STTChunk chunk;
                    chunk.set_corr_id(corr_id);
                    chunk.set_result(output);
                    chunk.set_is_partial_chunk(false);
                    chunk.set_is_result_chunk(false);
                    chunk.set_is_final_chunk(true);
                    WriteSTTChunk(wsi, chunk, ldi);
                    
                });
            }
        if (ldi->live_stt)
            ReplyIfAny(wsi, ldi);
        dynamic_batcher_->Push(corr_id, start, end, wave_part);
        return kSuccess;
        }
    return -1;
    }

static int
callback_audio_streaming(struct lws* wsi,
    enum lws_callback_reasons reason,
    void* user, void* in, size_t len)
{

    struct audio_streaming_protocol_instance* ldi = (audio_streaming_protocol_instance*)user;
    unsigned char* writable_buffer = &ldi->buf[LWS_SEND_BUFFER_PRE_PADDING];
    
    switch (reason) {
    case LWS_CALLBACK_PROTOCOL_INIT: {
       break;

    }
    case LWS_CALLBACK_PROTOCOL_DESTROY: {
        break;
    }

    case LWS_CALLBACK_ESTABLISHED: {
        //auto now = std::chrono::system_clock::now();
        //std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        //std::cout << "the last connection established at " << std::ctime(&end_time) << std::endl;
        ldi->writable_size = 0;// initialize send buffer
        ldi->live_stt = false;
        break;
    }

    case LWS_CALLBACK_CLOSED: {
        //lwsl_user("LWS_CALLBACK_CLOSED....\n");
        //auto now = std::chrono::system_clock::now();
        //std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        //std::cout << "the last connection closed at " << std::ctime(&end_time) << std::endl;
        break;
    }

    case LWS_CALLBACK_RECEIVE:
        try {
            char* buffer = (char*)in;
            std::memcpy(ldi->buffer + ldi->total_bytes_received, buffer, len);
            ldi->total_bytes_received += len;
            if (lws_is_final_fragment(wsi) && !lws_remaining_packet_payload(wsi)) { //EOC event
                WavChunk chunk;
                size_t size = ldi->total_bytes_received;
                ldi->total_bytes_received = 0;
                if(chunk.ParseFromArray(ldi->buffer, size)) // convert to batch message 
                {
                    if (Execute(wsi, chunk, ldi) != kSuccess)
                        return -1;
                }
                else
                {
                    std::cout << "Error in parsing the chunk...." << std::endl;
                }
            }
            else
            {
               // std::cout << "fragment...." << std::endl;
            }
        }
        catch (const std::exception& e) {
            std::cerr << e.what();
        }
        break;
    case LWS_CALLBACK_SERVER_WRITEABLE: {
        
        if (latest_sent_msg_was_buffered) {
            if (lws_partial_buffered(wsi)) {
                latest_sent_msg_was_buffered = true;
                lws_callback_on_writable(wsi);
                cout << "latest_sent_msg_was_buffered in server " << endl;
                break;
            }
            else
            { 
                latest_sent_msg_was_buffered = false;
            }
            break;
        }
        

        int n = 0;
        {
            std::lock_guard<std::mutex> lk(writer_m_);
            n = lws_write(wsi, writable_buffer, ldi->writable_size, LWS_WRITE_BINARY);
            ldi->writable_size = 0;// reset to indicate the write done.
        }
        
#ifdef lws_verbose
        
        if (n < 0)
            std::cout << "Error Sending for size = " << writable_size_ << std::endl;
        else if (n < writable_size_)
            std::cout << "Partial write" << std::endl;
        else if (n == writable_size_) 
            std::cout << "Write successful " << writable_size_ / 1024.0 <<"(KB)" << std::endl;
#endif
        if (lws_partial_buffered(wsi)) {
            latest_sent_msg_was_buffered = true;
            cout << "latest sent msg was_buffered in server " << n / 1024.0 << "(KB)" << std::endl;
            lws_callback_on_writable(wsi);
            break;
        }
        break;

    }

    default:
        break;
    }
    return 0;
}

static struct lws_protocols protocols[] = {
    // first protocol must always be HTTP handler //
    {
        "audio-streaming-protocol", // protocol name - very important!
        callback_audio_streaming,   // callback
        sizeof(struct audio_streaming_protocol_instance), // per_session_data_size
        65536 * 8, // rx_buffer_size
    0,    // id
    NULL, // user
    0     // tx_packet_size
    },
    {
        NULL, NULL, 0, 0   /* End of list */
    }
};

void sigint_handler(int sig)
{
    interrupted = 1;
}

/***************************
*   Logging
*
***************************/
LWS_VISIBLE LWS_EXTERN void lwsl_emit_stderr(int level,
    const char* line
)
{
    std::cout << "LWS_LOG():" << line << std::endl;
}
//******************
//*** EXECUTABLE ***
//******************


int main(int argc, char* argv[]) {
    try {

#ifdef MEMORY_LEAKS_DETECTION
        _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

        // Verify that the version of the library that we linked against is
        // compatible with the version of the headers we compiled against.
        GOOGLE_PROTOBUF_VERIFY_VERSION;

        // KALDI INITIALIZATION START

        using namespace kaldi;
        using namespace fst;

        typedef kaldi::int32 int32;
        typedef kaldi::int64 int64;



        int errno;
        if ((errno = SetUpAndReadCmdLineOptions(argc, argv, &opts_))) return errno;

        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        nnet3::Nnet xvector_nnet;
        fst::Fst<fst::StdArc>* decode_fst;
#ifdef XVECTOR_SD 
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_,
            &xvector_nnet, &xvector_mean_, &xvector_transform_, &plda_, &xvector_computer_);
#else
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_);
#endif
        cuda_pipeline_ = new BatchedThreadedNnet3CudaOnlinePipeline(
            opts_.batched_decoder_config, *decode_fst, am_nnet, trans_model);
        delete decode_fst;
        if (word_syms_) cuda_pipeline_->SetSymbolTable(*word_syms_);

        CudaOnlinePipelineDynamicBatcherConfig dynamic_batcher_config;
        dynamic_batcher_ = new CudaOnlinePipelineDynamicBatcher(dynamic_batcher_config, *cuda_pipeline_);

        nvtxRangePush(L"Global Timer");

        // KALDI INITIALIZATION FINISHED

        // COMMUNICATION LAYER INITIALIZATION

        struct lws_context_creation_info info;
        struct lws_context* context;
        int n = 0, logs = LLL_USER
            /*LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE*/
            /* for LLL_ verbosity above NOTICE to be built into lws,
            * lws must have been configured and built with
            * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
            /* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
            /* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
            /* | LLL_DEBUG */;

        signal(SIGINT, sigint_handler);
        lws_set_log_level(logs, lwsl_emit_stderr);
        //lws_set_log_level(logs, NULL);

        lwsl_user("\n -------- REVCORD LWS server ----------\n");

        memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
        info.port = port;
        info.protocols = protocols;
        info.pt_serv_buf_size = 1024 * 1024 * 8; // 8M
        info.options = LWS_SERVER_OPTION_VALIDATE_UTF8 |
            LWS_SERVER_OPTION_HTTP_HEADERS_SECURITY_BEST_PRACTICES_ENFORCE;
        
        
        context = lws_create_context(&info);
        if (context == NULL) {
            KALDI_LOG << "libwebsocket init failed";
            return -1;
        }

        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);

        std::cout << std::endl << "Server started at " << std::ctime(&end_time) << std::endl;

        // infinite loop, to end this server send SIGTERM. (CTRL+C)
        while (n >= 0 && !interrupted)
            n = lws_service(context, 0);
        lws_context_destroy(context);
        lwsl_user("Completed %s\n", interrupted == 2 ? "OK" : "failed");

        dynamic_batcher_->WaitForCompletion();

        nvtxRangePop();
        delete word_syms_;  // will delete if non-NULL.
        delete cuda_pipeline_;
        delete dynamic_batcher_;
        cudaDeviceSynchronize();

        // Optional:  Delete all global objects allocated by libprotobuf.
        google::protobuf::ShutdownProtobufLibrary();

#ifdef MEMORY_LEAKS_DETECTION
        _CrtDumpMemoryLeaks();
#endif

        return interrupted != 2;
    }
    catch (const std::exception& e) {
        std::cerr << e.what();
        return -1;
    }
}  // main()

#endif  // if HAVE_CUDA == 1
