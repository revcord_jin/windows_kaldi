//
//  batched-xvector-computer.cc
//  
//  Created by Jin Zhe Feng on 10/2/21.
//  Copyright © 2021 Revcord, Inc. All rights reserved.
//

#include "batched-xvector-computer.h"
using namespace kaldi;
using namespace std;
using namespace nnet3;



/**
    This function divides the number 'a' into 'b' pieces, such that
    the sum of the pieces equals 'a' and no two pieces differ by more
    than 1.
        @param [in] a     A number, may be positive or negative
        @param [in] b     The number of pieces, b >= 1.
        @param [out] pieces   The pieces will be written to here.
                        At exit, their sum will equal a, and none
                        of them will differ from any other by more
                        than 1.  Otherwise they are arbitrarily
                        chosen.
    */
void DivideIntoPieces(int32 a, int32 b, std::vector<int32>* pieces) {
    KALDI_ASSERT(b > 0);
    pieces->clear();
    pieces->reserve(b);
    int32 a_sign = 1;
    // Make sure a is positive before division, because the behavior of division
    // with negative operands is not fully defined in C.
    if (a < 0) {
        a_sign = -1;
        a *= -1;
    }
    int32 piece_size1 = a / b,
        piece_size2 = piece_size1 + 1,
        remainder = a % b;
    int32 num_pieces_of_size1 = b - remainder,
        num_pieces_of_size2 = remainder;
    KALDI_ASSERT(a == num_pieces_of_size1 * piece_size1 +
        num_pieces_of_size2 * piece_size2);

    for (int32 i = 0; i < num_pieces_of_size1; i++)
        pieces->push_back(piece_size1 * a_sign);
    for (int32 i = 0; i < num_pieces_of_size2; i++)
        pieces->push_back(piece_size2 * a_sign);
}

BatchedXvectorComputer::XvectorTask*
BatchedXvectorComputer::CreateTask(
    const std::string& utt, int32 num_chunks) {
    XvectorTask* task = new XvectorTask;
    task->utt_id = utt;
    task->num_chunks = num_chunks;
    task->num_chunks_finished = 0;
    task->xvector.Resize(xvector_dim_);
    task->tail = NULL;
    if (results_tail_) {
        results_tail_->tail = task;
        results_tail_ = task;
    }
    else {  // List was previously empty.
        results_head_ = task;
        results_tail_ = task;
    }
    return task;
}

BatchedXvectorComputer::BatchedXvectorComputer(
    const BatchedXvectorComputerOptions& opts,
    const Nnet& nnet,
    int32 total_context) :
    opts_(opts),
    total_context_(total_context),
    nnet_(nnet),
    position_in_batch_(0),
    results_head_(NULL),
    results_tail_(NULL) {

    tasks_this_batch_.resize(opts_.batch_size);

    feature_dim_ = nnet.InputDim("input");
    xvector_dim_ = nnet.OutputDim("output");
    // Zero input_feats_ in case there is only one batch, to avoid
    // NaN's being generated due to undefined data.
    input_feats_.Resize(opts_.chunk_size * opts_.batch_size,
        feature_dim_);

    CachingOptimizingCompiler compiler(nnet, opts.optimize_config,
        opts.compiler_config);

    {  // This block creates computation_.
        ComputationRequest request;
        request.need_model_derivative = false;
        request.store_component_stats = false;
        request.inputs.resize(1);
        IoSpecification& input(request.inputs[0]);
        input.name = "input";
        input.has_deriv = false;
        input.indexes.resize(opts_.batch_size * opts_.chunk_size);
        // Note: the sequences are interleaved in the input; this will save an extra
        // copy since it corresponds to how nnet3 stores things by default.  (Makes
        // TDNNs easier to implement.)
        for (int32 n = 0; n < opts_.batch_size; n++) {
            for (int32 t = 0; t < opts_.chunk_size; t++) {
                Index index;
                index.n = n;
                index.t = t;
                // index.x is 0 by default.
                input.indexes[n + opts_.batch_size * t] = index;
            }
        }
        IoSpecification output;
        output.name = "output";
        output.has_deriv = false;
        output.indexes.resize(opts_.batch_size);
        for (int32 n = 0; n < opts_.batch_size; n++) {
            Index index;
            index.n = n;
            index.t = 0;
            output.indexes[n] = index;
        }
        request.outputs.push_back(output);
        computation_ = compiler.Compile(request);
    }
}

void BatchedXvectorComputer::AddChunkToBatch(
    XvectorTask* task,
    const Matrix<BaseFloat>& input,
    int32 chunk_start) {
    int32 n = position_in_batch_++;
    KALDI_ASSERT(n >= 0 && n < opts_.batch_size);
    tasks_this_batch_[n] = task;
    int32 T = opts_.chunk_size,
        num_input_frames = input.NumRows();
    KALDI_ASSERT(input_feats_.NumRows() == T * opts_.batch_size);
    if (input.NumCols() != feature_dim_) {
        KALDI_ERR << "Feature dimension mismatch: neural net expected "
            << feature_dim_ << ", got " << input.NumCols();
    }
    for (int32 t = 0; t < T; t++) {
        SubVector<BaseFloat> dest(input_feats_, t * opts_.batch_size + n);
        int32 src_t = t + chunk_start;
        if (src_t >= num_input_frames) {
            KALDI_ASSERT(opts_.pad_input);
            src_t = num_input_frames - 1;  // Pad with repeats of the last frame.
        }
        SubVector<BaseFloat> src(input, src_t);
        dest.CopyFromVec(src);
    }
}

bool BatchedXvectorComputer::XvectorReady() const {
    if (results_head_ == NULL)
        return false;
    KALDI_ASSERT(results_head_->num_chunks_finished <= results_head_->num_chunks);
    return results_head_->num_chunks_finished == results_head_->num_chunks;
}

void BatchedXvectorComputer::OutputXvector(std::string* utt,
    Vector<BaseFloat>* xvector) {
    KALDI_ASSERT(XvectorReady());
    *utt = results_head_->utt_id;
    xvector->Swap(&(results_head_->xvector));
    XvectorTask* new_tail = results_head_->tail;
    delete results_head_;
    results_head_ = new_tail;
    if (new_tail == NULL)
        results_tail_ = NULL;
}

void BatchedXvectorComputer::Flush() {
    if (position_in_batch_ == 0)
        return;
    ComputeOneBatch();
}


void BatchedXvectorComputer::ComputeOneBatch() {

    CuMatrix<BaseFloat> cu_input_feats(input_feats_);
    Nnet* nnet_to_update = NULL;  // we're not doing any update.
    NnetComputer computer(opts_.compute_config, *computation_,
        nnet_, nnet_to_update);
    computer.AcceptInput("input", &cu_input_feats);
    computer.Run();
    CuMatrix<BaseFloat> cu_output;
    computer.GetOutputDestructive("output", &cu_output);
    KALDI_ASSERT(cu_output.NumRows() == opts_.batch_size);
    Matrix<BaseFloat> output(cu_output);
    for (int32 n = 0; n < opts_.batch_size; n++) {
        XvectorTask* task = tasks_this_batch_[n];
        if (task == NULL)
            continue;  // Would only happen for the last batch.
        task->num_chunks_finished++;
        task->xvector.AddVec(1.0 / task->num_chunks, output.Row(n));
    }
    position_in_batch_ = 0;
    std::fill(tasks_this_batch_.begin(), tasks_this_batch_.end(),
        (XvectorTask*)NULL);
}

void BatchedXvectorComputer::AcceptUtterance(
    const std::string& utt,
    const Matrix<BaseFloat>& input) {
    std::vector<int32> chunk_starts;
    int32 num_frames = input.NumRows();
    SplitUtteranceIntoChunks(num_frames, &chunk_starts);
    int32 num_chunks = chunk_starts.size();
    XvectorTask* task = CreateTask(utt, num_chunks);

    for (int32 i = 0; i < num_chunks; i++) {
        AddChunkToBatch(task, input, chunk_starts[i]);
        if (position_in_batch_ == opts_.batch_size) {
            ComputeOneBatch();
        }
    }
}

void BatchedXvectorComputer::SplitUtteranceIntoChunks(
    int32 num_frames, std::vector<int32>* start_frames) {
    start_frames->clear();
    if (num_frames <= opts_.chunk_size) {
        if (num_frames == opts_.chunk_size || opts_.pad_input)
            start_frames->push_back(0);
        // if we leave start_frames empty, then we just won't compute anything for
        // this file.
    }
    else {
        // these modified quantities are to account for the context effects...  when
        // the chunks overlap by exactly total_context_, the frames that get
        // averaged by the respective chunks in their averaging layers would touch
        // but not overlap.  So the optimal separation between chunks would equal
        // opts_.chunk_size - total_context_.
        int32 modified_num_frames = num_frames - total_context_,
            modified_chunk_size = opts_.chunk_size - total_context_;
        KALDI_ASSERT(modified_num_frames > modified_chunk_size);
        int32 num_chunks1 = modified_num_frames / modified_chunk_size,
            num_chunks2 = num_chunks1 + 1;
        int32 num_frames1 = num_chunks1 * modified_chunk_size,
            num_frames2 = num_chunks2 * modified_chunk_size;
        KALDI_ASSERT(num_frames2 > modified_chunk_size);
        // The M and N below correspond to the M and N in the comment:
        // M is the number of frames repeated once in the averaging, N
        // the number of frames repeated twice.  (Basically a solution
        // of the equations: (M + 2N == num_frames2, M+N == modified_num_frames).
        // Note: by a "frame" above, I mean a specific "t" value in
        // the utterance.
        int32 N = num_frames2 - modified_num_frames,
            M = modified_num_frames - N;
        KALDI_ASSERT(M + 2 * N == num_frames2 && M + N == modified_num_frames);

        // The variances below are proportional to the variance of our
        // estimate of the xvector under certain simplifying assumptions..
        // they help us choose whether to have gaps between the chunks
        // or overlaps between them.
        BaseFloat variance1 = 1.0 / num_frames1,  // the 1/M mentioned above.
            variance2 = (M + 4.0 * N) / ((M + 2.0 * N) * (M + 2.0 * N));
        if (variance1 <= variance2) {
            // We'll choose the smaller number of chunks.  There may be gaps.
            // Counting the positions at the ends, there are num_chunks+1 positions
            // where there might be gaps.
            // Note: "total_gap" is >= 0, it's the positive of the sum of the
            // sizes of those gaps.
            int32 num_chunks = num_chunks1,
                num_gaps = num_chunks + 1,
                total_gap = modified_num_frames - num_chunks * modified_chunk_size;
            KALDI_ASSERT(0 <= total_gap && total_gap < modified_chunk_size);
            std::vector<int32> gap_sizes;  // elements will be >= 0.
            DivideIntoPieces(total_gap, num_gaps, &gap_sizes);
            int32 pos = gap_sizes[0];
            for (int32 i = 0; i < num_chunks; i++) {
                start_frames->push_back(pos);
                pos += modified_chunk_size + gap_sizes[i + 1];
            }
            KALDI_ASSERT(pos == modified_num_frames);
        }
        else {
            int32 num_chunks = num_chunks2,
                num_overlaps = num_chunks - 1,
                total_overlap = modified_num_frames - num_chunks * modified_chunk_size;
            KALDI_ASSERT(-modified_chunk_size < total_overlap&& total_overlap <= 0);
            std::vector<int32> overlap_sizes;  // elements will be <= 0.
            DivideIntoPieces(total_overlap, num_overlaps, &overlap_sizes);
            int32 pos = 0;
            for (int32 i = 0; i < num_chunks; i++) {
                start_frames->push_back(pos);
                pos += modified_chunk_size;
                if (i < num_overlaps)
                    pos += overlap_sizes[i];
            }
            KALDI_ASSERT(pos == modified_num_frames);
        }
    }
}





