// kaldi_recognizer_test.cc

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.



#include <queue>
#if HAVE_CUDA == 1

#include <iostream>
#include <chrono>
#include <string>
#include <iomanip>
#include <sstream>
//#include <algorithm>
#include <sys/stat.h>
#include <stdio.h>
#include "revcord_api.h"
#include "feat/wave-reader.h"
#include "feat/wave-reader.h"
#include "util/kaldi-table.h"
#include <random>

#include <numeric>
#include <nvToolsExt.h>
#include <cuda_runtime_api.h>
#include "icallbackhandler.h"

using namespace std;
using namespace kaldi;

struct Stream {
    std::shared_ptr<WaveData> wav;
    uint64_t corr_id;
    int offset;
    double send_next_chunk_at;
    double* latency_ptr;
    KaldiRecognizer *recognizer;

    Stream(const std::shared_ptr<WaveData>& _wav,
        uint64_t _corr_id,
        double _send_next_chunk_at, double* _latency_ptr, KaldiRecognizer *_recognizer)
        : wav(_wav),
        corr_id(_corr_id),
        offset(0),
        send_next_chunk_at(_send_next_chunk_at),
        latency_ptr(_latency_ptr),
        recognizer(_recognizer){}

    bool operator<(const Stream& other) const {
        return (send_next_chunk_at > other.send_next_chunk_at);
    }
};

// Prints some statistics based on latencies stored in latencies
void PrintLatencyStats(std::vector<double>& latencies) {
    if (latencies.empty()) return;
    double total = std::accumulate(latencies.begin(), latencies.end(), 0.);
    double avg = total / latencies.size();
    std::sort(latencies.begin(), latencies.end());

    double nresultsf = static_cast<double>(latencies.size());
    size_t per90i = static_cast<size_t>(std::floor(90. * nresultsf / 100.));
    size_t per95i = static_cast<size_t>(std::floor(95. * nresultsf / 100.));
    size_t per99i = static_cast<size_t>(std::floor(99. * nresultsf / 100.));

    double lat_90 = latencies[per90i];
    double lat_95 = latencies[per95i];
    double lat_99 = latencies[per99i];

    KALDI_LOG << "Latencies (s):\tAvg\t\t90%\t\t95%\t\t99%";
    KALDI_LOG << std::fixed << std::setprecision(3) << "\t\t\t" << avg << "\t\t"
        << lat_90 << "\t\t" << lat_95 << "\t\t" << lat_99;
}




void ReadDataset(std::vector<std::shared_ptr<WaveData>>* all_wav,
                    std::vector<std::string>* all_wav_keys) {
    // pre-loading data
    // we don't want to measure I/O
    SequentialTableReader<WaveHolder> wav_reader("scp:F:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp");
    {
        KALDI_LOG << "Loading eval dataset...";
        for (; !wav_reader.Done(); wav_reader.Next()) {
            std::string utt = wav_reader.Key();
            std::shared_ptr<WaveData> wave_data = std::make_shared<WaveData>();
            wave_data->Swap(&wav_reader.Value());
            all_wav->push_back(wave_data);
            all_wav_keys->push_back(utt);
            // total_audio += wave_data->Duration();
        }
        KALDI_LOG << "done\n";
    }
}

class mycallback : public ICallbackHandler
{
    void latticehandle(const int corr_id, const const char* str) {
        //std::cout << "corr_id #" << corr_id << " [final] : " << str << std::endl;
    };
    void bestpathhandle(const int corr_id, const char* str, const bool endpoint_detected) {
       // if(endpoint_detected)
       //     std::cout << "corr_id #" << corr_id << " [endpoint] : " << str << std::endl;
       // else
       //     std::cout << "corr_id #" << corr_id << " [partial] : " << str << std::endl;
    };
};

int main(int argc, char *argv[]) {
try {
    typedef kaldi::int32 int32;
    typedef kaldi::int64 int64;
    revcord_api api;
    Model* stt_model = api.revcord_stt_model_new(argc, argv);

    int chunk_length = api.revcord_stt_get_chunk_length(stt_model);// 4080;
    double chunk_seconds = api.revcord_stt_get_chunk_seconds(stt_model);
    double seconds_per_sample = chunk_seconds / chunk_length;
    
    std::vector<std::shared_ptr<WaveData>> all_wav;
    std::vector<std::string> all_wav_keys;
    ReadDataset(&all_wav, &all_wav_keys);

    // Streaming code
    // Wav reader index
    size_t all_wav_i = 0;
    size_t all_wav_max = all_wav.size() * 1;
    std::vector<double> latencies(all_wav_max);

    // We will start all utterances at a random position within the first second
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0);

    std::priority_queue<Stream> streams;
    nvtxRangePush(L"Global Timer");
    Timer timer;

    // Initial set of streams will start randomly within the first second of
    // streaming That's to simulate something more realistic than a large spike
    // of all channels starting at the same time
    bool add_random_offset = false;// true;
    KALDI_LOG << "Inferencing...";
    double duration = 0.0;
    uint64_t corr_id;

    int loop_count = 0;
    while (true) {
        int num_streaming_channels =api.revcord_stt_num_streaming_channels(stt_model);
        
        while (streams.size() < num_streaming_channels) {
            int wav_i = all_wav_i++;
            if (wav_i >= all_wav_max) {
                all_wav_i = all_wav_max;
                break;
            }
            if (wav_i % num_streaming_channels == 0)
                cout << "I am alive..." << std::endl;
            
            size_t all_wav_i_modulo = wav_i % (all_wav.size());
            
            KaldiRecognizer* recognizer = api.revcord_recognizer_new(stt_model, 8000.0);
            mycallback* callback = new mycallback();
            corr_id = api.revcord_recognizer_get_corr_id(recognizer);
            api.revcord_recognizer_setupcallback(recognizer, callback);

            // The "speaker" start talking at stream_will_start_at
            double stream_will_start_at = api.revcord_recognizer_timer_elapsed(recognizer);  // "now"
            
            if (add_random_offset) stream_will_start_at += dis(gen);
            // The "speaker" will speak for stream_duration seconds
            double stream_duration = all_wav[all_wav_i_modulo]->Duration();
            duration += stream_duration;
            // The first chunk will be available whenever the first chunk was
            // "spoken" e.g. if the first chunk is made of 0.5s for audio, we have
            // to wait 0.5s after stream_will_start_at
            double first_chunk_available_at =
                stream_will_start_at + std::min(stream_duration, chunk_seconds);

            // stream_will_stop_at is used for latency computation
            // We started streaming at t0=stream_will_start_at, so the audio will be
            // done streaming at t1=stream_will_start_at+duration We will get
            // the result at t2, we then have latency=t2-t1
            double stream_will_stop_at = stream_will_start_at + stream_duration;
            // tmp storing in lat_ptr
            // we'll do lat_ptr = now - lat_ptr in callback
            
            // Adding that stream to our simulation stream pool

            
            double* latency_ptr = &latencies[corr_id];
            *latency_ptr = stream_will_stop_at;
            streams.emplace(all_wav[all_wav_i_modulo], corr_id,
                first_chunk_available_at, latency_ptr, recognizer);
        }
        add_random_offset = false;  // the next streams will just start whenever a
                                    // spot is available

        // If we reach this, we're just done with all streams
        if (streams.empty()) break;

        auto chunk = streams.top();
        streams.pop();
        double wait_for = chunk.send_next_chunk_at - api.revcord_recognizer_timer_elapsed(chunk.recognizer);
        if (wait_for > 0) api.revcord_recognizer_usleep(chunk.recognizer, wait_for * 1e6);


        SubVector<BaseFloat> data(chunk.wav->Data(), 0);

        // Current chunk
        int32 total_num_samp = data.Dim();
        
        int this_chunk_num_samp =
            std::min(total_num_samp - chunk.offset, chunk_length);
        bool is_last_chunk =
            ((chunk.offset + this_chunk_num_samp) == total_num_samp);
        bool is_first_chunk = (chunk.offset == 0);
        SubVector<BaseFloat> wave_part(data, chunk.offset, this_chunk_num_samp);
        // Giving current chunk to the dynamic batcher for processing
        api.revcord_recognizer_accept_waveform_f(chunk.recognizer, wave_part.Data(), wave_part.Dim(), 
                                chunk.corr_id, is_first_chunk, is_last_chunk);
        // Current chunk was sent, done
        chunk.offset += this_chunk_num_samp;

        // Streaming simulation:
        // We need to know the duration of the next chunk
        // The next time we will "wake up" the stream will be at
        // send_next_chunk_at with send_next_chunk_at = send_current_chunk_at +
        // next_chunk_duration
        int next_chunk_num_samp =
            std::min(total_num_samp - chunk.offset, chunk_length);
        double next_chunk_seconds = next_chunk_num_samp * seconds_per_sample;
        chunk.send_next_chunk_at += next_chunk_seconds;

        // If there is a next chunk, add it to the list of streams tasks
        if (!is_last_chunk) streams.push(chunk);
        //else cout << "Close a recognizer with corr_id=" << chunk.corr_id << std::endl;
    }
    
    api.revcord_stt_model_waitforcompletion(stt_model);
    KALDI_LOG << "Done.";
    nvtxRangePop();

    double total_time = timer.Elapsed();

    KALDI_LOG << "Total Audio: " << duration
        << " seconds, Total Time: " << total_time
        << " seconds, RTFX: " << duration / total_time
        << " Total correlation id: " << (corr_id + 1)
        << " Total audio tested : expected " << all_wav_i << " : " << all_wav_max;

    KALDI_LOG << "\tLatency stats:";
    PrintLatencyStats(latencies);
    cudaDeviceSynchronize();
    
   // revcord_stt_model_free(stt_model);
    return 0;
    } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}  // main()

#endif  // if HAVE_CUDA == 1

