// Copyright 2021 Revcord Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* This header contains the C API for Revcord speech recognition system */

#ifndef REVCORD_API_H
#define REVCORD_API_H

#include "kaldi_recognizer.h"

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/** Callback object is the main object which processes recognition result.
 *  Each recognizer usually runs in own thread and outputs result as callback */
typedef struct ICallbackHandler ICallbackHandler;

#define REVCORD_API __declspec(dllexport)


class REVCORD_API revcord_api
{
public:
	revcord_api(void) {};
	~revcord_api(void) {};

	/** Loads stt model data from the file and returns the model object
	 *
	 * @param model_path: the path of the model on the filesystem
	 @ @returns model object */
	Model* revcord_stt_model_new(int argc, char* argv[]);

	/** Releases the stt model memory
	 *
	 *  The model object is reference-counted so if some recognizer
	 *  depends on this model, model might still stay alive. When
	 *  last recognizer is released, model will be released too. */
	void revcord_stt_model_free(Model* model);

	/** Waits for completion of batch processing in stt model
	 *
	 *  dynamic_batcher wait for completion. */
	void revcord_stt_model_waitforcompletion(Model* model);

	/** get num_streaming_channels in stt model
	 *
	 *  num_streaming channels. */
	int revcord_stt_num_streaming_channels(Model* model);

	/** get chunk_length in stt model
	 *
	 *  chunk_length. */
	int revcord_stt_get_chunk_length(Model* model);

	/** get chunk_seconds in stt model
	 *
	 *  chunk_seconds. */
	double revcord_stt_get_chunk_seconds(Model* model);

	/** Creates the recognizer object
	 *
	 *  The recognizers process the speech and return text using shared stt & sd model data
	 *  @param sample_rate The sample rate of the audio you going to feed into the recognizer
	 *  @returns recognizer object */
	KaldiRecognizer* revcord_recognizer_new(Model* stt_model, float sample_rate);


	/** Creates the recognizer object with the phrase list
	 *
	 *  Sometimes when you want to improve recognition accuracy and when you don't need
	 *  to recognize large vocabulary you can specify a list of phrases to recognize. This
	 *  will improve recognizer speed and accuracy but might return [unk] if user said
	 *  something different.
	 *
	 *  Only recognizers with lookahead models support this type of quick configuration.
	 *  Precompiled HCLG graph models are not supported.
	 *
	 *  @param sample_rate The sample rate of the audio you going to feed into the recognizer
	 *  @param grammar The string with the list of phrases to recognize as JSON array of strings,
	 *                 for example "["one two three four five", "[unk]"]".
	 *
	 *  @returns recognizer object */
	KaldiRecognizer* revcord_recognizer_new_grm(Model* stt_model, float sample_rate, const char* grammar);


	/** Initialize the recognizer's state to be ready for processing new audio.
	 *  @returns void */
	void revcord_recognizer_initialize(KaldiRecognizer* recognizer);

	/** Setup CallbackHandler of the recognizer to be ready for processing results.
	 *  @returns void */
	void revcord_recognizer_setupcallback(KaldiRecognizer* recognizer, ICallbackHandler* callback);

	typedef unsigned long long uint64_t;
	/** Accept voice data
	 *
	 *  accept and process new chunk of voice data
	 *
	 *  @param data - audio data in PCM 16-bit mono format
	 *  @param length - length of the audio data
	 *  @returns true if silence is occured and you can retrieve a new utterance with result method */
	int revcord_recognizer_accept_waveform(KaldiRecognizer* recognizer, const char* data, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk);

	/** Same as above but the version with the short data for language bindings where you have
	 *  audio as array of shorts */
	int revcord_recognizer_accept_waveform_s(KaldiRecognizer* recognizer, const short* sdata, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk);


	/** Same as above but the version with the float data for language bindings where you have
	 *  audio as array of floats */
	int revcord_recognizer_accept_waveform_f(KaldiRecognizer* recognizer, const float* fdata, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk);


	/** Returns speech recognition result
	 *
	 * @returns the result in JSON format which contains decoded line, decoded
	 *          words, times in seconds and confidences. You can parse this result
	 *          with any json parser
	 *
	 * <pre>
	 * {
	 *   "result" : [{
	 *       "conf" : 1.000000,
	 *       "end" : 1.110000,
	 *       "start" : 0.870000,
	 *       "word" : "what"
	 *     }, {
	 *       "conf" : 1.000000,
	 *       "end" : 1.530000,
	 *       "start" : 1.110000,
	 *       "word" : "zero"
	 *     }, {
	 *       "conf" : 1.000000,
	 *       "end" : 1.950000,
	 *       "start" : 1.530000,
	 *       "word" : "zero"
	 *     }, {
	 *       "conf" : 1.000000,
	 *       "end" : 2.340000,
	 *       "start" : 1.950000,
	 *       "word" : "zero"
	 *     }, {
	 *       "conf" : 1.000000,
	 *      "end" : 2.610000,
	 *       "start" : 2.340000,
	 *       "word" : "one"
	 *     }],
	 *   "text" : "what zero zero zero one"
	 *  }
	 * </pre>
	 */
	const char* revcord_recognizer_result(KaldiRecognizer* recognizer);


	/** Returns partial speech recognition
	 *
	 * @returns partial speech recognition text which is not yet finalized.
	 *          result may change as recognizer process more data.
	 *
	 * <pre>
	 * {
	 *  "partial" : "cyril one eight zero"
	 * }
	 * </pre>
	 */
	const char* revcord_recognizer_partial_result(KaldiRecognizer* recognizer);


	/** Returns speech recognition result. Same as result, but doesn't wait for silence
	 *  You usually call it in the end of the stream to get final bits of audio. It
	 *  flushes the feature pipeline, so all remaining audio chunks got processed.
	 *
	 *  @returns speech result in JSON format.
	 */
	const char* revcord_recognizer_final_result(KaldiRecognizer* recognizer);


	/** Releases recognizer object
	 *
	 *  Underlying model is also unreferenced and if needed released */
	void revcord_recognizer_free(KaldiRecognizer* recognizer);

	/** Set log level for Kaldi messages
	 *
	 *  @param log_level the level
	 *     0 - default value to print info and error messages but no debug
	 *     less than 0 - don't print info messages
	 *     greather than 0 - more verbose mode
	 */
	void revcord_recognizer_set_log_level(KaldiRecognizer* recognizer, int log_level);

	/** Get corr_id for a chunk processing
	 *
	*/
	uint64_t revcord_recognizer_get_corr_id(KaldiRecognizer* recognizer);

	/** Get elapsed time from the creation of model
	 *
	*/
	double revcord_recognizer_timer_elapsed(KaldiRecognizer* recognizer);

	/** delay in microseconds
	 *
	*/
	void revcord_recognizer_usleep(KaldiRecognizer* recognizer, uint64_t usec);

};


#ifdef __cplusplus
}
#endif

#endif /* REVCORD_API_H */

