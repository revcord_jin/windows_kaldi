// kaldi_recognizer_test.cc

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#if HAVE_CUDA == 1

#include "revcord_api.h"
#include "icallbackhandler.h"
#include "libwebsockets.h"
int main(int argc, char *argv[]) {

    revcord_api api;
    Model* stt_model = api.revcord_stt_model_new(argc, argv);
    int chunk_length = api.revcord_stt_get_chunk_length(stt_model);// 4080;
    double chunk_seconds = api.revcord_stt_get_chunk_seconds(stt_model);
    double seconds_per_sample = chunk_seconds / chunk_length;
    api.revcord_stt_model_waitforcompletion(stt_model);
    api.revcord_stt_model_free(stt_model);

}  // main()

#endif  // if HAVE_CUDA == 1

