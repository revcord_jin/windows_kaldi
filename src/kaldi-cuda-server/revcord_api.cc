// Copyright 2020 Alpha Cephei Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "revcord_api.h"
#include <string.h>

Model* revcord_api::revcord_stt_model_new(int argc, char* argv[])
{
    return new Model(argc, argv);
}

void revcord_api::revcord_stt_model_free(Model* model)
{
    model->Unref();
}

void revcord_api::revcord_stt_model_waitforcompletion(Model* model)
{
    model->WaitForCompletion();
}

int revcord_api::revcord_stt_num_streaming_channels(Model* model)
{
    return model->GetNumStreamingChannels();
}

int revcord_api::revcord_stt_get_chunk_length(Model* model)
{
    return model->GetChunkLength();
}

double revcord_api::revcord_stt_get_chunk_seconds(Model* model)
{
    return model->GetChunkSeconds();
}


KaldiRecognizer* revcord_api::revcord_recognizer_new(Model* stt_model, float sample_rate)
{
    return new KaldiRecognizer(stt_model, sample_rate);
}


KaldiRecognizer* revcord_api::revcord_recognizer_new_grm(Model* stt_model, float sample_rate, const char* grammar)
{
    return new KaldiRecognizer(stt_model, sample_rate);
}

void revcord_api::revcord_recognizer_initialize(KaldiRecognizer* recognizer)
{
    recognizer->Initialize();
}

void revcord_api::revcord_recognizer_setupcallback(KaldiRecognizer* recognizer, ICallbackHandler* callback)
{
    recognizer->SetupCallback(callback);
}

int revcord_api::revcord_recognizer_accept_waveform(KaldiRecognizer* recognizer, const char* data, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    return recognizer->AcceptWaveform(data, length, corr_id, is_first_chunk, is_last_chunk);
}

int revcord_api::revcord_recognizer_accept_waveform_s(KaldiRecognizer* recognizer, const short* sdata, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    return recognizer->AcceptWaveform(sdata, length, corr_id, is_first_chunk, is_last_chunk);
}

int revcord_api::revcord_recognizer_accept_waveform_f(KaldiRecognizer* recognizer, const float* fdata, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    return recognizer->AcceptWaveform(fdata, length, corr_id, is_first_chunk, is_last_chunk);
}

const char* revcord_api::revcord_recognizer_result(KaldiRecognizer* recognizer)
{
    return recognizer->Result();
}

const char* revcord_api::revcord_recognizer_partial_result(KaldiRecognizer* recognizer)
{
    return recognizer->PartialResult();
}

const char* revcord_api::revcord_recognizer_final_result(KaldiRecognizer* recognizer)
{
    return recognizer->FinalResult();
}

void revcord_api::revcord_recognizer_free(KaldiRecognizer* recognizer)
{
    delete recognizer;
}

void revcord_api::revcord_recognizer_set_log_level(KaldiRecognizer* recognizer, int log_level)
{
    recognizer->SetVerboseLevel(log_level);
}
uint64_t revcord_api::revcord_recognizer_get_corr_id(KaldiRecognizer* recognizer)
{
    return recognizer->GetCorrID();
}
double revcord_api::revcord_recognizer_timer_elapsed(KaldiRecognizer* recognizer)
{
    return recognizer->TimerElapsed();
}
void revcord_api::revcord_recognizer_usleep(KaldiRecognizer* recognizer, uint64_t usec)
{
    recognizer->microSleep(usec);
}

