%module (directors="1","threads"=1) revcordstt

%include <typemaps.i>
%include <windows.i>
%include <arrays_csharp.i>

CSHARP_ARRAYS(char, byte)
%apply char INPUT[] { const char *data , const char *grammar };
%apply float INPUT[] {const float *fdata};
%apply short INPUT[] {const short *sdata};

CSHARP_ARRAYS(char *, string)
%apply char *INPUT[]  { char *argv[] }

%{
typedef struct Model Model;
typedef struct KaldiRecognizer KaldiRecognizer;
#include "revcord_api.h"
#include "icallbackhandler.h"
%}



%include "revcord_api.h"
// generate directors for all classes that have virtual methods
%feature("director") ICallbackHandler; 
%include "icallbackhandler.h"

typedef struct {} Model;
typedef struct {} KaldiRecognizer;






