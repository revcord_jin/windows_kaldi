// model.h
//
//  
//  Created by Jin Zhe Feng on 15/5/20.
//  Copyright � 2020 Revcord, Inc. All rights reserved.
//


#ifndef model_H_
#define model_H_


#include "cudadecoder/cuda-online-pipeline-dynamic-batcher.h"
#include "fstext/fstext-lib.h"
#include "nnet3/am-nnet-simple.h"
#include "nnet3/nnet-utils.h"
#include "batched-xvector-computer.h"

using namespace std;
using namespace kaldi;
using namespace cuda_decoder;
using namespace nnet3;
using namespace fst;

class KaldiRecognizer;

class Model {

public:
    Model(int argc, char* argv[]);
    Model() {};
    ~Model();
    void Ref();
    void Unref();
    void WaitForCompletion();
    int GetNumStreamingChannels() { return num_streaming_channels;}
    int GetChunkLength() { return chunk_length;}
    double GetChunkSeconds() { return chunk_seconds; }

    Timer timer;
    // STT members
    SymbolTable* word_syms;
    

    // SD members
    Vector<BaseFloat>* xvector_mean;
    Matrix<BaseFloat>* xvector_transform;
    Plda* plda;

protected:
    friend class KaldiRecognizer;
    std::atomic<int> ref_cnt_{ 0 };

    int SetUpAndReadCmdLineOptions(int argc, char* argv[]);
    
    // STT members
    int chunk_length;
    double chunk_seconds;
    double seconds_per_sample;
    int num_streaming_channels;

    TransitionModel trans_model;
    AmNnetSimple am_nnet;
    
    string word_syms_rxfilename, nnet3_rxfilename, fst_rxfilename;
    BatchedThreadedNnet3CudaOnlinePipelineConfig batched_decoder_config;
    std::atomic<BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID> correlation_id_cnt{ 0 };
    BatchedThreadedNnet3CudaOnlinePipeline* cuda_pipeline;
    CudaOnlinePipelineDynamicBatcher* dynamic_batcher;
   
    // SD members
    Nnet xvector_nnet;
    BatchedXvectorComputerOptions batched_xvector_opts;
    string xvector_model_path_rxfilename;
    string sd_model_path_rxfilename;
    BatchedXvectorComputer* xvector_computer;
    BaseFloat target_energy_;
    int32 total_context;

};

#endif /* model_H_ */
