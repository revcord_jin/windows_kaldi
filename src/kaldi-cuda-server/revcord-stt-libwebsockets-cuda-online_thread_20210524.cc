// cudadecoderbin/batched-wav-nnet3-cuda-online.cc
//
// Copyright (c) 2019, NVIDIA CORPORATION.  All rights reserved.
// Hugo Braun
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//#define lws_verbose
#include <queue>

//#define MEMORY_LEAKS_DETECTION

#ifdef MEMORY_LEAKS_DETECTION
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#if HAVE_CUDA == 1

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <nvToolsExt.h>
#include <iomanip>
#include "cudadecoder/batched-threaded-nnet3-cuda-online-pipeline.h"
#include "cudadecoder/cuda-online-pipeline-dynamic-batcher.h"
#include "cudadecoderbin/cuda-bin-tools.h"
//#include "cudadecoderbin/custom_backend.h"
#include "cudadecoderbin/cuda-speaker-diarization-tools.h"
#include "cudamatrix/cu-allocator.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "nnet3/am-nnet-simple.h"
#include "nnet3/nnet-utils.h"
#include "util/kaldi-thread.h"
#include "lat/sausages.h"
#include "json.h"

#include <iostream>
#include <istream>
#include <streambuf>
#include <string>


//Web socket-related includes
#define LWS_DLL
#define LWS_INTERNAL
#include "libwebsockets.h"
#include <signal.h>
#pragma comment(lib, "websockets.lib") // comment out when ubuntu
#pragma comment(lib, "libprotobuf.lib") // comment out when ubuntu
//#pragma comment(lib, "libprotobufd.lib") // comment out when ubuntu

#include "wavchunk.pb.h"
using namespace revcord;
using namespace std;


enum ErrorCodes {
    kSuccess,
    kUnknown,
    kInvalidModelConfig,
    kGpuNotSupported,
    kSequenceBatcher,
    kModelControl,
    kInputOutput,
    kInputName,
    kOutputName,
    kInputOutputDataType,
    kInputContents,
    kInputSize,
    kOutputBuffer,
    kBatchTooBig,
    kTimesteps,
    kChunkTooBig
};

using namespace std;
using namespace kaldi;
using namespace cuda_decoder;

CudaOnlineBinaryOptions opts_;
BatchedThreadedNnet3CudaOnlinePipeline* cuda_pipeline_ = NULL;
CudaOnlinePipelineDynamicBatcher* dynamic_batcher_ = NULL;

fst::SymbolTable* word_syms_;
std::unordered_set<BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID> is_corr_id_in_batch_;
std::vector<BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID> batch_corr_ids_;
std::vector<kaldi::SubVector<kaldi::BaseFloat>> batch_wave_samples_;
std::vector<bool> batch_is_last_chunk_;
std::vector<bool> batch_is_first_chunk_;

bool latest_sent_msg_was_buffered = false;
// Reply with ....
unsigned char buf[LWS_PRE + 65536 * 8 + LWS_SEND_BUFFER_POST_PADDING];
unsigned char* writable_buffer = &buf[LWS_PRE];


std::mutex writer_m_;
size_t writable_size_ = 0;
//Result STT relevant connection
struct lws* sttWSI;


// Contants 
const uint64_t int32_byte_size_ = 4;
const uint64_t int64_byte_size_ = 8;
const uint64_t chunk_num_bytes_ = 8160;
const uint64_t chunk_num_samps_ = 4080;// 8000 * 0.51

#ifdef XVECTOR_SD
Vector<BaseFloat> xvector_mean_;
Matrix<BaseFloat> xvector_transform_;
Plda plda_;
BatchedXvectorComputer* xvector_computer_;

std::string SpeakerDiarization(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat,
    const Matrix<BaseFloat>& h_utt_features,
    const BatchedXvectorComputer& xvector_computer,
    const Vector<BaseFloat>& xvector_mean,
    const Matrix<BaseFloat>& xvector_transform,
    const Plda& plda) {

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    for (int i = 0; i < size; i++) {
        json::JSON word;
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }

    std::vector<Vector<BaseFloat>> xvectors = xvector_computation(xvector_computer, h_utt_features, word_sequence);
    bool has_xvector = (xvectors.size() > 0);
    std::vector<int32> spk_ids;
    int num_speakers = VBDiarization(opts.batched_xvector_opts, xvector_mean, xvector_transform, plda, xvectors, spk_ids);
    std::string out_str = MakeFinalJSON(has_xvector, spk_ids, num_speakers, word_sequence);
    return out_str;
}
#endif

std::string BuildFinalResult(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat) 
{

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    json::JSON word;
    for (int i = 0; i < size; i++) {
        
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }
    std::string out_str = MakeFinalJSONOnlyForSTT(word_sequence);
    return out_str;
}



int FlushBatch() {
    if (!batch_corr_ids_.empty()) {
        cuda_pipeline_->DecodeBatch(batch_corr_ids_, batch_wave_samples_,
            batch_is_first_chunk_,
            batch_is_last_chunk_);
        is_corr_id_in_batch_.clear(); 
        batch_corr_ids_.clear();
        batch_is_first_chunk_.clear();
        batch_is_last_chunk_.clear();
        batch_wave_samples_.clear();
    }
    return 0;
}






//*****************************
//*** AUDIO STREAMING LAYER ***
//*****************************

//Server configuration definitions
#define TIME_INTERVAL 50
const int DEFAULT_SERVER_PORT = 2571;
static int interrupted, port = DEFAULT_SERVER_PORT;

// audio stream buffers
const int MAX_NUM_CHANNELS = 1024;

/* one of these created for each message in the ringbuffer */

struct msg {
    void* payload; /* is malloc'd */
    size_t len;
};

/*
 * This runs under both lws service and "cuda_pipeline threads" contexts.
 * Access is serialized by vhd->lock_ring.
 */

static void
__minimal_destroy_message(void* _msg)
{
    struct msg* msg = (struct msg* )_msg;
    free(msg->payload);
    msg->payload = NULL;
    msg->len = 0;
}

/*
 * One of these is created for each client connecting to us.
 *
 * It is ONLY read or written from the lws service thread context.
 */

struct per_session_data__minimal {
    struct per_session_data__minimal* pss_list;
    struct lws* wsi;
    uint32_t tail;
    char buffer[chunk_num_bytes_ + sizeof(WavChunk)];
    BaseFloat wav[chunk_num_samps_];
    uint64_t total_bytes_received;
};

/* one of these is created for each vhost our protocol is used with */

struct per_vhost_data__minimal {
    struct lws_context* context;
    struct lws_vhost* vhost;
    const struct lws_protocols* protocol;

    struct per_session_data__minimal* pss_list; /* linked-list of live pss*/

    std::mutex lock_ring; /* serialize access to the ring buffer */
    struct lws_ring* ring; /* {lock_ring} ringbuffer holding unsent content */

    const char* config;
    char finished;
};

/*
 * This demonstrates how to pass a pointer into a specific protocol handler
 * running on a specific vhost.  In this case, it's our default vhost and
 * we pass the pvo named "config" with the value a const char * "myconfig".
 *
 * This is the preferred way to pass configuration into a specific vhost +
 * protocol instance.
 */

static const struct lws_protocol_vhost_options pvo_ops = {
    NULL,
    NULL,
    "config",		/* pvo name */
    "myconfig"	/* pvo value */
};

static const struct lws_protocol_vhost_options pvo = {
    NULL,		/* "next" pvo linked-list */
    &pvo_ops,	/* "child" pvo linked-list */
    "revcord-streaming-stt",	/* protocol name we belong to on this vhost */
    ""		/* ignored */
};

bool WriteSTTChunk(struct lws* wsi,struct per_vhost_data__minimal* vhd, STTChunk chunk)
{

    struct msg amsg;
    int len, n;
    {
        //std::lock_guard<std::mutex> lk(vhd->lock_ring);
        /* only create if space in ringbuffer */
        do
        {
            {
                std::lock_guard<std::mutex> lk(writer_m_);
                n = (int)lws_ring_get_count_free_elements(vhd->ring);
            }
            if (n != 0)
                break;
            else
            {
                lwsl_user("There is not any free element in the ring, waiting 1s!\n");
                usleep(1000000);
            }
        } while (true);

        len = chunk.ByteSizeLong();
        
        do
        {
            /* wait until there is some free memory to allocate payload */
            do
            {
                amsg.payload = malloc((unsigned int)(LWS_PRE + len + LWS_SEND_BUFFER_POST_PADDING));
                if (!amsg.payload) {
                    lwsl_user("There is not free memory, waiting 1s for others to free memory!\n");
                    usleep(1000000);
                }
                else break;
            } while (true);

            chunk.SerializeToArray((char*)amsg.payload + LWS_PRE, len);
            amsg.len = (unsigned int)len;

            {
                std::lock_guard<std::mutex> lk(writer_m_);
                if (chunk.is_final_chunk())
                    std::cout << " chunk for corr_id = " << chunk.corr_id() << "  total_bytes = " << len << std::endl;
                n = (int)lws_ring_insert(vhd->ring, &amsg, 1);
            }

            if (n == 1) {
                
                /*
                * This will cause a LWS_CALLBACK_EVENT_WAIT_CANCELLED
                * in the lws service thread context.
                */
                lws_cancel_service(vhd->context);
                //lws_callback_on_writable(wsi);
                break;
                
            }
            else {
                __minimal_destroy_message(&amsg);
                lwsl_user("Fails to insert new msg, waiting 1s !\n");
                usleep(1000000);
            }
        } while (true);
    }
    return true;
}

bool EmptyReply(struct lws* wsi, struct per_vhost_data__minimal* vhd) {
    std::string output = "{\"text\": \"\"}";
    STTChunk chunk;
    chunk.set_corr_id(-1);
    chunk.set_is_partial_chunk(false);
    chunk.set_is_result_chunk(false);
    chunk.set_is_final_chunk(false);
    chunk.set_result(output);
    return WriteSTTChunk(wsi, vhd, chunk);
}

int Execute(struct lws* wsi, struct per_vhost_data__minimal* vhd, WavChunk chunk, BaseFloat* wav) {

    if (Info_Kind::Info_Kind_REFRESH == chunk.info().kind())
    {
        cuda_pipeline_->FreeChannelMap();
        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        EmptyReply(wsi, vhd);
        std::cout << "Initialized the server " << std::ctime(&end_time) << std::endl;
        return kSuccess;
    }
    else if (Info_Kind::Info_Kind_GETSTT == chunk.info().kind())
    {
        return kSuccess;
    }
    else if (Info_Kind::Info_Kind_WAV == chunk.info().kind())
    {

        int32_t length = chunk.samples().length() / 2;
        short* wav_short = (short*)(chunk.samples().data());
        for (int i = 0; i < length; i++)
            wav[i] = (BaseFloat)wav_short[i];
        SubVector<BaseFloat> wave_part(wav, length);

        bool start = chunk.is_first_chunk();
        bool end = chunk.is_last_chunk();
        BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID corr_id = chunk.corr_id();
        if (start) {
            // Defining the callback for the results
            cuda_pipeline_->SetBestPathCallback(
                corr_id,
                [corr_id, vhd](
                    const std::string& str, bool partial, bool endpoint_detected) {

                        STTChunk chunk;
                        json::JSON res;
                        if (endpoint_detected)
                        {
                            res["text"] = str;
                            chunk.set_corr_id(corr_id);
                            chunk.set_is_partial_chunk(false);
                            chunk.set_is_result_chunk(true);
                            chunk.set_is_final_chunk(false);
                            chunk.set_result(res.dump());
                        }
                        else if (partial) {
                            res["partial"] = str;
                            chunk.set_corr_id(corr_id);
                            chunk.set_is_partial_chunk(true);
                            chunk.set_is_result_chunk(false);
                            chunk.set_is_final_chunk(false);
                            chunk.set_result(res.dump());
                        }
                        // WriteSTTChunk(wsi, vhd, chunk);
                });
        }
        if (end) {
            // If last chunk, set the callback for that seq
            cuda_pipeline_->SetLatticeCallback(
#ifdef XVECTOR_SD
                corr_id, [corr_id](CompactLattice& clat, Matrix<BaseFloat>& h_utt_features) {
                    std::string output = SpeakerDiarization(opts_, *word_syms_, clat, h_utt_features, *xvector_computer_, xvector_mean_, xvector_transform_, plda_);
                    //std::string output = "{\"text\": \"\"}";
#else
                corr_id, [corr_id, wsi, vhd](CompactLattice& clat) {
                    //std::string output = "{\"text\": \"\"}";
                    std::string output = BuildFinalResult(opts_, *word_syms_, clat);
#endif

                    STTChunk chunk;
                    chunk.set_corr_id(corr_id);
                    chunk.set_result(output);
                    chunk.set_is_partial_chunk(false);
                    chunk.set_is_result_chunk(false);
                    chunk.set_is_final_chunk(true);
                    WriteSTTChunk(wsi, vhd, chunk);
                });
            }
        //EmptyReply(vhd);
        dynamic_batcher_->Push(corr_id, start, end, wave_part);
        return kSuccess;
        }
    return -1;
    }




static int
callback_revcord_stt(struct lws* wsi,
    enum lws_callback_reasons reason,
    void* user, void* in, size_t len)
{

    struct per_session_data__minimal* pss =
        (struct per_session_data__minimal*)user;
    struct per_vhost_data__minimal* vhd =
        (struct per_vhost_data__minimal*)
        lws_protocol_vh_priv_get(lws_get_vhost(wsi),
            lws_get_protocol(wsi));
    const struct lws_protocol_vhost_options* pvo;
    const struct msg* pmsg;
    void* retval;
    int n, m, r = 0;
    
    switch (reason) {
    case LWS_CALLBACK_PROTOCOL_INIT: {

        /* create our per-vhost struct */
        vhd = (struct per_vhost_data__minimal* )lws_protocol_vh_priv_zalloc(lws_get_vhost(wsi),
            lws_get_protocol(wsi),
            sizeof(struct per_vhost_data__minimal));
        if (!vhd)
            return 1;

        /* recover the pointer to the globals struct */
        pvo = lws_pvo_search(
            (const struct lws_protocol_vhost_options*)in,
            "config");
        if (!pvo || !pvo->value) {
            lwsl_err("%s: Can't find \"config\" pvo\n", __func__);
            return 1;
        }

        vhd->config = pvo->value;

        vhd->context = lws_get_context(wsi);
        vhd->protocol = lws_get_protocol(wsi);
        vhd->vhost = lws_get_vhost(wsi);

        vhd->ring = lws_ring_create(sizeof(struct msg), 2048,
            __minimal_destroy_message);
        if (!vhd->ring) {
            lwsl_err("%s: failed to create ring\n", __func__);
            return 1;
        }




        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << "The Vhost context had been created at " << std::ctime(&end_time) << std::endl;
        break;

    }
    case LWS_CALLBACK_PROTOCOL_DESTROY: {
        
        vhd->finished = 1;

        if (vhd->ring)
            lws_ring_destroy(vhd->ring);

        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << "The Vhost context had been destroyed at " << std::ctime(&end_time) << std::endl;
        break;
    }

    case LWS_CALLBACK_ESTABLISHED: {
        /* add ourselves to the list of live pss held in the vhd */
        lws_ll_fwd_insert(pss, pss_list, vhd->pss_list);
        pss->tail = lws_ring_get_oldest_tail(vhd->ring);
        pss->wsi = wsi;
        break;
    }

    case LWS_CALLBACK_CLOSED: {
        /* remove our closing pss from the list of live pss */
        lws_ll_fwd_remove(struct per_session_data__minimal, pss_list,
            pss, vhd->pss_list);
        break;
    }

    case LWS_CALLBACK_RECEIVE:
        try {
            char* buffer = (char*)in;
            std::memcpy(pss->buffer + pss->total_bytes_received, buffer, len);
            pss->total_bytes_received += len;
            if (lws_is_final_fragment(wsi) && !lws_remaining_packet_payload(wsi)) { //EOC event
                WavChunk chunk;
                size_t size = pss->total_bytes_received;
                pss->total_bytes_received = 0;
                if(chunk.ParseFromArray(pss->buffer, size)) // convert to batch message 
                {
                    if (Execute(pss->wsi, vhd, chunk, pss->wav) != kSuccess)
                        return -1;
                }
                else
                {
                    std::cout << "Error in parsing the chunk...." << std::endl;
                }
                
            }
            else
            {
               // std::cout << "fragment...." << std::endl;
            }
        }
        catch (const std::exception& e) {
            std::cerr << e.what();
        }
        break;
    case LWS_CALLBACK_SERVER_WRITEABLE: {
            {
                std::lock_guard<std::mutex> lk(writer_m_);
                pmsg = (const struct msg*)lws_ring_get_element(vhd->ring, &pss->tail);
            }

            if (!pmsg) {
                break;
            }

            /* notice we allowed for LWS_PRE in the payload already */
            m = lws_write(wsi, ((unsigned char*)pmsg->payload) + LWS_PRE,
                pmsg->len, LWS_WRITE_BINARY);
            if (m < (int)pmsg->len) {
                lwsl_err("ERROR %d writing to ws socket\n", m);
                return -1;
            }

            {
                std::lock_guard<std::mutex> lk(writer_m_);
                lws_ring_consume_and_update_oldest_tail(
                    vhd->ring,	/* lws_ring object */
                    struct per_session_data__minimal, /* type of objects with tails */
                    &pss->tail,	/* tail of guy doing the consuming */
                    1,		/* number of payload objects being consumed */
                    vhd->pss_list,	/* head of list of objects with tails */
                    tail,		/* member name of tail in objects with tails */
                    pss_list	/* member name of next object in objects with tails */
                );
                if (lws_ring_get_element(vhd->ring, &pss->tail))
                    /* come back as soon as we can write more */
                    lws_callback_on_writable(pss->wsi);
            }

            

        break;
    }
    case LWS_CALLBACK_EVENT_WAIT_CANCELLED:

        if (!vhd)
            break;
        /*
         * When the "spam" threads add a message to the ringbuffer,
         * they create this event in the lws service thread context
         * using lws_cancel_service().
         *
         * We respond by scheduling a writable callback for all
         * connected clients.
         */
        lws_start_foreach_llp(struct per_session_data__minimal**,
            ppss, vhd->pss_list) {
            lws_callback_on_writable((*ppss)->wsi);
        } lws_end_foreach_llp(ppss, pss_list);
        break;
    default:
        break;
    }
    return 0;
}

static struct lws_protocols protocols[] = {
    // first protocol must always be HTTP handler //
    {
        "revcord-streaming-stt", // protocol name - very important!
        callback_revcord_stt,   // callback
        sizeof(struct per_session_data__minimal), // per_session_data_size
        65536 * 8, // rx_buffer_size
    0,    // id
    NULL, // user
    0     // tx_packet_size
    },
    {
        NULL, NULL, 0, 0   /* End of list */
    }
};

void sigint_handler(int sig)
{
    interrupted = 1;
}

/***************************
*   Logging
*
***************************/
LWS_VISIBLE LWS_EXTERN void lwsl_emit_stderr(int level,
    const char* line
)
{
    std::cout << "LWS_LOG():" << line << std::endl;
}
//******************
//*** EXECUTABLE ***
//******************


int main(int argc, char* argv[]) {
    try {

#ifdef MEMORY_LEAKS_DETECTION
        _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

        // Verify that the version of the library that we linked against is
        // compatible with the version of the headers we compiled against.
        GOOGLE_PROTOBUF_VERIFY_VERSION;

        // KALDI INITIALIZATION START

        using namespace kaldi;
        using namespace fst;

        typedef kaldi::int32 int32;
        typedef kaldi::int64 int64;



        int errno;
        if ((errno = SetUpAndReadCmdLineOptions(argc, argv, &opts_))) return errno;

        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        nnet3::Nnet xvector_nnet;
        fst::Fst<fst::StdArc>* decode_fst;
#ifdef XVECTOR_SD 
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_,
            &xvector_nnet, &xvector_mean_, &xvector_transform_, &plda_, &xvector_computer_);
#else
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_);
#endif
        cuda_pipeline_ = new BatchedThreadedNnet3CudaOnlinePipeline(
            opts_.batched_decoder_config, *decode_fst, am_nnet, trans_model);
        delete decode_fst;
        if (word_syms_) cuda_pipeline_->SetSymbolTable(*word_syms_);

        CudaOnlinePipelineDynamicBatcherConfig dynamic_batcher_config;
        dynamic_batcher_ = new CudaOnlinePipelineDynamicBatcher(dynamic_batcher_config, *cuda_pipeline_);

        nvtxRangePush(L"Global Timer");

        // KALDI INITIALIZATION FINISHED

        // COMMUNICATION LAYER INITIALIZATION

        struct lws_context_creation_info info;
        struct lws_context* context;
        int n = 0, logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE
            /*LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE*/
            /* for LLL_ verbosity above NOTICE to be built into lws,
            * lws must have been configured and built with
            * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
            /* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
            /* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
            /* | LLL_DEBUG */;

        signal(SIGINT, sigint_handler);
        lws_set_log_level(logs, lwsl_emit_stderr);
        //lws_set_log_level(logs, NULL);
        lwsl_user("REVCORD LWS server\n");

        memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
        info.port = port;
        info.protocols = protocols;
        info.pvo = &pvo; /* per-vhost options */
        info.options = LWS_SERVER_OPTION_VALIDATE_UTF8 |
            LWS_SERVER_OPTION_HTTP_HEADERS_SECURITY_BEST_PRACTICES_ENFORCE;
        
        /*
        info.ka_time = 3600;//3600*24
        info.ka_interval = 60;
        info.ka_probes = 10;
        */
                
        context = lws_create_context(&info);
        if (context == NULL) {
            KALDI_LOG << "libwebsocket init failed";
            return -1;
        }

        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << "Server started at " << std::ctime(&end_time) << std::endl;

        // infinite loop, to end this server send SIGTERM. (CTRL+C)
        while (n >= 0 && !interrupted)
            n = lws_service(context, 0);
        lws_context_destroy(context);
        lwsl_user("Completed %s\n", interrupted == 2 ? "OK" : "failed");

        dynamic_batcher_->WaitForCompletion();

        nvtxRangePop();
        delete word_syms_;  // will delete if non-NULL.
        delete cuda_pipeline_;
        delete dynamic_batcher_;
        cudaDeviceSynchronize();

        // Optional:  Delete all global objects allocated by libprotobuf.
        google::protobuf::ShutdownProtobufLibrary();

#ifdef MEMORY_LEAKS_DETECTION
        _CrtDumpMemoryLeaks();
#endif

        return interrupted != 2;
    }
    catch (const std::exception& e) {
        std::cerr << e.what();
        return -1;
    }
}  // main()

#endif  // if HAVE_CUDA == 1
