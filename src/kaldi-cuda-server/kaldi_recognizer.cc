// kaldi_recognizer.cc
//
//  
//  Created by Jin Zhe Feng on 15/5/20.
//  Copyright � 2020 Revcord, Inc. All rights reserved.
//
#include <queue>
#if HAVE_CUDA == 1
#include "revcord_api.h"
#include "kaldi_recognizer.h"
#include "ivector/agglomerative-clustering.h"
#include "json.h"


KaldiRecognizer::KaldiRecognizer(Model* model, float sample_rate) : stt_model_(model), sample_rate_(sample_rate) {

    stt_model_->Ref();
    corr_id = stt_model_->correlation_id_cnt++;
    cuda_pipeline = stt_model_->cuda_pipeline;
    dynamic_batcher = stt_model_->dynamic_batcher;
    word_syms = stt_model_->word_syms;
    xvector_mean = stt_model_->xvector_mean;
    xvector_transform = stt_model_->xvector_transform;
    plda = stt_model_->plda;
    xvector_computer = stt_model_->xvector_computer;
	batched_xvector_opts = stt_model_->batched_xvector_opts;
    is_first_chunk = false;
    is_last_chunk = false;
    partial_result = "";
    endpoint_result = "";
    final_result = "";
}

KaldiRecognizer::~KaldiRecognizer() {
    stt_model_->Unref();
}

void KaldiRecognizer::SetVerboseLevel(int log_level)
{
    kaldi::SetVerboseLevel(log_level);
}


void KaldiRecognizer::Initialize()
{
}

uint64_t KaldiRecognizer::GetCorrID()
{
    return corr_id;
}

void KaldiRecognizer::SetupCallback(ICallbackHandler* handler)
{
    if (handler)
    {
        this->CallbackHandler = handler;


        double* latency_ptr = &latency;
        string* final_result_ptr = &final_result;
        string* result_ptr = &endpoint_result;
        string* partial_result_ptr = &partial_result;

        uint64_t corr_id = this->corr_id;

        // Defining the callback for results

        cuda_pipeline->SetBestPathCallback(
            corr_id,
            [this, latency_ptr, partial_result_ptr, result_ptr, corr_id](
                const std::string& str, bool partial, bool endpoint_detected) {
                    if (partial) {
                        //KALDI_LOG << "corr_id #" << corr_id << " [partial] : " << str;
                        *partial_result_ptr = str;
                        //cout << "corr_id #" << corr_id << " [partial] : " << str << std::endl;
                        
                    }
                    if (endpoint_detected) {
                        //KALDI_LOG << "corr_id #" << corr_id << " [endpoint detected]" << str;
                        *result_ptr = str;
                        //cout << std::endl << "corr_id #" << corr_id << " [endpoint detected] : " << *result_ptr << std::endl;
                    }
                    CallbackHandler->bestpathhandle(corr_id, str.c_str(), endpoint_detected);
            });

        //KALDI_LOG << "SetLatticeCallback for corr_id:" << corr_id;

        cuda_pipeline->SetLatticeCallback(
            corr_id,
#ifdef XVECTOR_SD
            [this, latency_ptr, final_result_ptr, corr_id](CompactLattice& clat, Matrix<BaseFloat>& h_utt_features) {
                string str = SpeakerDiarization(clat, h_utt_features);
#else
            [this, latency_ptr, final_result_ptr, corr_id](CompactLattice& clat) {
                string str = "{\"text\": \"\"}";
#endif
                //*latency_ptr = timer.Elapsed() - *latency_ptr;
                
                *final_result_ptr = str;
                //cout << std::endl << "corr_id #" << this->corr_id << " : " << str << std::endl;
                //string str = "Lattice Callback done....";
                CallbackHandler->latticehandle(corr_id, str.c_str());
                
            });

    }
    else
    {
        std::cout << "CallbackHandler object is null" << endl;
    }
    
}

bool KaldiRecognizer::AcceptWaveform(const char* data, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    Vector<BaseFloat> wave;
    wave.Resize(len / 2, kUndefined);
    for (int i = 0; i < len / 2; i++)
        wave(i) = *(((short*)data) + i);
    SubVector<BaseFloat> wave_part(wave, 0, len/2);
    return AcceptWaveform(wave_part, corr_id, is_first_chunk, is_last_chunk);
}

bool KaldiRecognizer::AcceptWaveform(const short* sdata, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    Vector<BaseFloat> wave;
    wave.Resize(len, kUndefined);
    for (int i = 0; i < len; i++)
        wave(i) = sdata[i];
    SubVector<BaseFloat> wave_part(wave, 0, len);
    return AcceptWaveform(wave_part, corr_id, is_first_chunk, is_last_chunk);
}

bool KaldiRecognizer::AcceptWaveform(const float* fdata, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    SubVector<BaseFloat> wave_part(fdata, len);
    return AcceptWaveform(wave_part, corr_id, is_first_chunk, is_last_chunk);
}
bool KaldiRecognizer::AcceptWaveform(SubVector<BaseFloat> wave_part, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    dynamic_batcher->Push(corr_id, is_first_chunk, is_last_chunk, wave_part);
    return false;
}

const char* KaldiRecognizer::Result()
{
    return endpoint_result.c_str();
}
const char* KaldiRecognizer::PartialResult()
{
    return partial_result.c_str();
}
const char* KaldiRecognizer::FinalResult()
{
    return final_result.c_str();
}

double KaldiRecognizer::TimerElapsed()
{
    {
        double value = stt_model_->timer.Elapsed();
        return value;
    }
}

void KaldiRecognizer::microSleep(uint64_t usec)
{
    usleep(usec);
}

std::vector<Vector<BaseFloat>> xvector_computation(BatchedXvectorComputer xvector_computer,
    const Matrix<BaseFloat>& input_features,
    const std::vector<std::tuple<std::string, int32, int32, BaseFloat>>& wordsequence) {
    std::vector<Vector<BaseFloat>> output_xvector;
    // Sliding Winodw for CMVN compensation
    SlidingWindowCmnOptions cmvn_opts_;
    cmvn_opts_.center = true;
    cmvn_opts_.normalize_variance = false;
    cmvn_opts_.cmn_window = 300;

    Matrix<BaseFloat> full_features(input_features.NumRows(), input_features.NumCols(), kUndefined);
    SlidingWindowCmn(cmvn_opts_, input_features, &full_features);
    KALDI_ASSERT(full_features.NumRows() > 0);
    int32 frame_count = 0;
    output_xvector.resize(wordsequence.size());

    for (int i = 0; i < wordsequence.size(); i++) {
        std::string utt = std::to_string(i);
        auto word = wordsequence[i];
        std::string word_text = std::get<0>(word);
        int32 word_start = std::get<1>(word);
        int32 word_end = std::get<2>(word);
        BaseFloat confidence = std::get<3>(word);
        Matrix<BaseFloat> features;
        features.Resize(word_end - word_start + 1, full_features.NumCols());
        for (int j = word_start, k = 0; j <= word_end; j++, k++)
            features.Row(k).CopyFromVec(full_features.Row(j));

        if (features.NumRows() == 0) {
            KALDI_WARN << "Zero-length utterance: " << utt;
            continue;
        }
        frame_count += features.NumRows();
        xvector_computer.AcceptUtterance(utt, features);
        while (xvector_computer.XvectorReady()) {
            std::string utt;
            Vector<BaseFloat> xvector;
            xvector_computer.OutputXvector(&utt, &xvector);
            int j; std::istringstream(utt) >> j;
            output_xvector.at(j) = xvector;
        }
    }

    xvector_computer.Flush();
    while (xvector_computer.XvectorReady()) {
        std::string utt;
        Vector<BaseFloat> xvector;
        xvector_computer.OutputXvector(&utt, &xvector);
        int j; std::istringstream(utt) >> j;
        output_xvector.at(j) = xvector;
    }
    return output_xvector;
}

bool EstPca(const Matrix<BaseFloat>& ivector_mat, BaseFloat target_energy,
    const std::string& reco, Matrix<BaseFloat>* mat) {

    // If the target_energy is 1.0, it's equivalent to not applying the
    // conversation-dependent PCA at all, so it's better to exit this
    // function before doing any computation.
    if (ApproxEqual(target_energy, 1.0, 0.001))
        return false;

    int32 num_rows = ivector_mat.NumRows(),
        num_cols = ivector_mat.NumCols();
    Vector<BaseFloat> sum;
    SpMatrix<BaseFloat> sumsq;
    sum.Resize(num_cols);
    sumsq.Resize(num_cols);
    sum.AddRowSumMat(1.0, ivector_mat);
    sumsq.AddMat2(1.0, ivector_mat, kTrans, 1.0);
    sum.Scale(1.0 / num_rows);
    sumsq.Scale(1.0 / num_rows);
    sumsq.AddVec2(-1.0, sum); // now sumsq is centered covariance.
    int32 full_dim = sum.Dim();

    Matrix<BaseFloat> P(full_dim, full_dim);
    Vector<BaseFloat> s(full_dim);

    try {
        if (num_rows > num_cols)
            sumsq.Eig(&s, &P);
        else
            Matrix<BaseFloat>(sumsq).Svd(&s, &P, NULL);
    }
    catch (...) {
        KALDI_WARN << "Unable to compute conversation dependent PCA for"
            << " recording " << reco << ".";
        return false;
    }

    SortSvd(&s, &P);

    Matrix<BaseFloat> transform(P, kTrans); // Transpose of P.  This is what
                                         // appears in the transform.

    // We want the PCA transform to retain target_energy amount of the total
    // energy.
    BaseFloat total_energy = s.Sum();
    BaseFloat energy = 0.0;
    int32 dim = 1;
    while (energy / total_energy <= target_energy) {
        energy += s(dim - 1);
        dim++;
    }
    Matrix<BaseFloat> transform_float(transform);
    mat->Resize(transform.NumCols(), transform.NumRows());
    mat->CopyFromMat(transform);
    mat->Resize(dim, transform_float.NumCols(), kCopyData);
    return true;
}

// Transforms i-vectors using the PLDA model.
void TransformIvectors(const Matrix<BaseFloat>& ivectors_in,
    const PldaConfig& plda_config, const Plda& plda,
    Matrix<BaseFloat>* ivectors_out) {
    int32 dim = plda.Dim();
    ivectors_out->Resize(ivectors_in.NumRows(), dim);
    for (int32 i = 0; i < ivectors_in.NumRows(); i++) {
        Vector<BaseFloat> transformed_ivector(dim);
        plda.TransformIvector(plda_config, ivectors_in.Row(i), 1.0,
            &transformed_ivector);
        ivectors_out->Row(i).CopyFromVec(transformed_ivector);
    }
}

// Transform the i-vectors using the recording-dependent PCA matrix.
void ApplyPca(const Matrix<BaseFloat>& ivectors_in,
    const Matrix<BaseFloat>& pca_mat, Matrix<BaseFloat>* ivectors_out) {
    int32 transform_cols = pca_mat.NumCols(),
        transform_rows = pca_mat.NumRows(),
        feat_dim = ivectors_in.NumCols();
    ivectors_out->Resize(ivectors_in.NumRows(), transform_rows);
    KALDI_ASSERT(transform_cols == feat_dim);
    ivectors_out->AddMatMat(1.0, ivectors_in, kNoTrans,
        pca_mat, kTrans, 0.0);
}


int VBDiarization(BatchedXvectorComputerOptions opts,
    const Vector<BaseFloat>& xvector_mean,
    const Matrix<BaseFloat>& xvector_transform,
    const Plda& plda,
    std::vector<Vector<BaseFloat>>& xvectors,
    std::vector<int32>& spk_ids) {

    //
    // ivector-subtract-global-mean $pldadir/mean.vec: global mean suntraction 
    //
    int num_speakers = 0;
    int32 size = xvectors.size();
    if (size == 0) {

        //KALDI_WARN << "Not producing output for recording "
        //           << " since no segments had XVectors";
        return num_speakers;
    }




    //
    // Step 1: Mean Normalization of X-vectors
    //

    // when there is a global mean
    for (int32 i = 0; i < size; i++)
        xvectors[i].AddVec(-1.0, xvector_mean);

    // subtraction in domain global mean 
    Vector<double> sum;
    int64 num_done = 0;

    for (size_t i = 0; i < size; i++) {
        if (sum.Dim() == 0)
            sum.Resize(xvectors[i].Dim());
        sum.AddVec(1.0, xvectors[i]);
        num_done++;
    }


    //KALDI_LOG << "Read " << num_done << " XVectors.";

    if (num_done != 0) {
        //KALDI_LOG << "Norm of XVector mean was " << (sum.Norm(2.0) / num_done);
        for (size_t i = 0; i < size; i++) {
            xvectors[i].AddVec(-1.0 / num_done, sum);
        }
    }


    //
    //  Step 2: Linear Transform for PCA analysis
    //
    // transform-vec $pldadir/transform.mat: PCA analysis 
    //
    Matrix<BaseFloat> transform(xvector_transform);
    int32 transform_rows = transform.NumRows(),
        transform_cols = transform.NumCols(),
        vec_dim;


    for (int32 i = 0; i < size; i++) {
        vec_dim = xvectors[i].Dim();
        Vector<BaseFloat> vec_out(transform_rows);
        if (transform_cols == vec_dim) {
            vec_out.AddMatVec(1.0, transform, kNoTrans, xvectors[i], 0.0);
        }
        else {
            if (transform_cols != vec_dim + 1) {
                KALDI_ERR << "Dimension mismatch: input vector has dimension "
                    << vec_dim << " and transform has " << transform_cols
                    << " columns.";
            }
            vec_out.CopyColFromMat(transform, vec_dim);
            vec_out.AddMatVec(1.0, transform.Range(0, transform.NumRows(),
                0, vec_dim), kNoTrans, xvectors[i], 1.0);
        }
        xvectors[i].Swap(&vec_out);
    }



    // Step3: Score PLDA : ivector-plda-scoring-dense

    //
    //    target_energy=0.1
    //
    BaseFloat target_energy = 0.1;
    PldaConfig plda_config;
    KALDI_ASSERT(target_energy <= 1.0);

    Plda this_plda(plda);

    Matrix<BaseFloat> ivector_mat(xvectors.size(), xvectors[0].Dim()),
        ivector_mat_pca,
        ivector_mat_plda,
        pca_transform,
        scores(xvectors.size(), xvectors.size());



    for (size_t i = 0; i < xvectors.size(); i++) {
        ivector_mat.Row(i).CopyFromVec(xvectors[i]);
    }


    if (EstPca(ivector_mat, target_energy, "this audio", &pca_transform)) {
        // Apply the PCA transform to the raw i-vectors.
        ApplyPca(ivector_mat, pca_transform, &ivector_mat_pca);

        // Apply the PCA transform to the parameters of the PLDA model.
        this_plda.ApplyTransform(Matrix<double>(pca_transform));

        // Now transform the i-vectors using the reduced PLDA model.
        TransformIvectors(ivector_mat_pca, plda_config, this_plda,
            &ivector_mat_plda);
    }
    else {
        // If EstPca returns false, we won't apply any PCA.
        TransformIvectors(ivector_mat, plda_config, this_plda,
            &ivector_mat_plda);
    }
    for (int32 i = 0; i < ivector_mat_plda.NumRows(); i++) {
        for (int32 j = 0; j < ivector_mat_plda.NumRows(); j++) {
            scores(i, j) = this_plda.LogLikelihoodRatio(Vector<double>(
                ivector_mat_plda.Row(i)), 1.0,
                Vector<double>(ivector_mat_plda.Row(j)));
        }
    }


    //
    // Step 4 : Main loop of AHC
    // agglomerative-cluster
    //


    BaseFloat threshold = -0.5,
        max_spk_fraction = 1.0;
    bool read_costs = false;
    int32 first_pass_max_utterances = std::numeric_limits<int16>::max();

    if (!read_costs)
        threshold = -threshold;


    // By default, the scores give the similarity between pairs of
    // utterances.  We need to multiply the scores by -1 to reinterpet
    // them as costs (unless --read-costs=true) as the agglomerative
    // clustering code requires.
    if (!read_costs)
        scores.Scale(-1);
    spk_ids.clear();
    if (opts.num_speakers > 0) {
        if (1.0 / opts.num_speakers <= max_spk_fraction && max_spk_fraction <= 1.0)
            AgglomerativeCluster(scores, std::numeric_limits<BaseFloat>::max(),
                opts.num_speakers, first_pass_max_utterances,
                max_spk_fraction, &spk_ids);
        else
            AgglomerativeCluster(scores, std::numeric_limits<BaseFloat>::max(),
                opts.num_speakers, first_pass_max_utterances,
                1.0, &spk_ids);
    }
    else
    {
        AgglomerativeCluster(scores, threshold, 1, first_pass_max_utterances,
            1.0, &spk_ids);
    }

    //
    // Step 5: Post processing of speaker id sequences
    //
    num_speakers = 0;
    for (int32 i = 0; i < spk_ids.size(); i++) {
        int32 label = spk_ids[i];
        if (num_speakers < label)
            num_speakers = label;
    }



    // cout << "Number of speakers =" << num_speakers << std::endl;  

    //
    // Speaker Label Updates for lower number early arrived at
    //

    int32 temp_number = num_speakers + 1;
    int32 start_pos = 0;
    for (int32 spk = 1; spk <= num_speakers; spk++) {
        int32 label = -1;
        for (int32 i = start_pos; i < spk_ids.size(); i++) {
            if (spk == spk_ids[i]) { start_pos++; break; }
            if (spk < spk_ids[i]) {
                label = spk_ids[i];
                break;
            }
        }
        if (label > spk) // update
        {
            for (int32 i = 0; i < spk_ids.size(); i++) {
                if (spk_ids[i] == label)
                    spk_ids[i] = temp_number;

            }
            for (int32 i = 0; i < spk_ids.size(); i++) {
                if (spk_ids[i] == spk)
                    spk_ids[i] = label;
            }
            for (int32 i = 0; i < spk_ids.size(); i++) {
                if (spk_ids[i] == temp_number)
                    spk_ids[i] = spk;
            }
        }
    }

    /*
    for (int32 i = 0; i < spk_ids_.size(); i++)
         cout << spk_ids_[i] << std::endl;
    */

    //
    // Main loop of VB diarization
    //
    return num_speakers;

}

std::string MakeFinalJSON(bool has_xvector,
    const std::vector<int32>& spk_ids,
    const int num_speakers,
    const std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence) {

    json::JSON finalobj;
    bool isNull = true;

    if (has_xvector)
    {


        int32 prev_id = -1;
        int32 seg_index = 0;
        std::vector<BaseFloat> speaker_start;
        std::vector<int32> speaker_id;
        for (auto spk : spk_ids) {
            auto word = word_sequence[seg_index++];
            int32 word_start = std::get<1>(word); // in frame
            if (spk != prev_id) { //speaker change point
                speaker_start.push_back(word_start * 0.01 - 0.03);// consideration mismatch between STT and SD frames
                speaker_id.push_back(spk);
                prev_id = spk;
            }
        }
        // last time
        speaker_start.push_back(std::numeric_limits<int32>::max());

        finalobj["id"] = "Revcord";
        finalobj["speakers"] = num_speakers;
        finalobj["callid"] = "555d150000d000801290002918a10000";
        finalobj["confidence"] = 0.995;

        std::stringstream fulltext;
        int32 size = speaker_start.size();

        for (int i = 0; i < size - 1; i++) {
            BaseFloat seg_start = speaker_start[i];
            BaseFloat seg_end = speaker_start[i + 1];

            json::JSON obj;
            std::stringstream who;
            who << "speaker" << speaker_id[i];
            obj["who"] = who.str();


            BaseFloat speaker_start_time = -1.0;
            BaseFloat speaker_end_time = -1.0;
            std::stringstream text;
            int32 word_count = 0;
            for (int32 word_index = 0; word_index < word_sequence.size(); word_index++)
            {
                auto word = word_sequence[word_index];
                std::string word_text = std::get<0>(word);
                BaseFloat word_start = std::get<1>(word) * 0.03; // in seconds
                BaseFloat word_end = std::get<2>(word) * 0.03;  // in frame offset
                BaseFloat confidence = std::get<3>(word);

                // if ((word_start >= seg_start && word_start <= seg_end) ||
                //     (word_start < seg_start && word_end >= seg_start)) {// compare start time
                if (word_end >= seg_start && word_end < seg_end) {

                    //add word to this speaker group


                    json::JSON word;
                    word["word"] = word_text;
                    word["start"] = word_start;
                    word["end"] = word_end;
                    word["conf"] = confidence;

                    obj["words"].append(word);

                    if (word_count) {
                        text << " ";
                        speaker_end_time = word_end;

                    }
                    else { // word_count = 0

                        speaker_end_time = word_end;
                        speaker_start_time = word_start;
                    }

                    text << word_text;
                    word_count++;
                }
            }

            if (word_count > 0)
            {
                fulltext << text.str() << " ";
                obj["what"] = text.str();
                json::JSON speaker_segment;
                std::stringstream time_start;
                time_start << speaker_start_time;
                speaker_segment["start"] = time_start.str();
                std::stringstream time_end;
                time_end << speaker_end_time;
                speaker_segment["end"] = time_end.str();
                obj["when"] = speaker_segment;
                finalobj["speech2text"].append(obj);
                isNull = false;
            }
            else {
                // KALDI_LOG << "Too short to align to this speaker ";
            }

        }
        finalobj["fulltranscript"] = fulltext.str();
    }

    if (isNull)
    {
        // null object to make postprocessing happy
        finalobj["fulltranscript"] = "";

        json::JSON obj;
        obj["what"] = "";
        json::JSON speaker_segment;
        speaker_segment["start"] = "0.0";
        speaker_segment["end"] = "0.0";

        obj["when"] = speaker_segment;

        json::JSON word;
        word["conf"] = 0.0;
        word["start"] = 0.0;
        word["end"] = 0.0;
        word["word"] = "";

        obj["words"].append(word);

        finalobj["speech2text"].append(obj);
        return finalobj.dump();
    }
    return finalobj.dump();
}


string KaldiRecognizer::SpeakerDiarization(const kaldi::CompactLattice& clat, const Matrix<BaseFloat>& h_utt_features)
{

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    BaseFloat sad_start = -1.0;
    BaseFloat sad_end = -1.0;
    BaseFloat frame_offset_ = 0.0;
    for (int i = 0; i < size; i++) {
        json::JSON word;
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms->Find(words[i]),
            word_start, word_end, conf[i]));
    }

    std::vector<Vector<BaseFloat>> xvectors = xvector_computation(*xvector_computer, h_utt_features, word_sequence);
    bool has_xvector = (xvectors.size() > 0);
    std::vector<int32> spk_ids;
    int num_speakers = VBDiarization(batched_xvector_opts, *xvector_mean, *xvector_transform, *plda, xvectors, spk_ids);
    std::string out_str = MakeFinalJSON(has_xvector, spk_ids, num_speakers, word_sequence);
    return out_str;
}

#endif  // if HAVE_CUDA == 1