// cudadecoderbin/batched-wav-nnet3-cuda-online.cc
//
// Copyright (c) 2019, NVIDIA CORPORATION.  All rights reserved.
// Hugo Braun
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//#define lws_verbose
#include <queue>

//#define MEMORY_LEAKS_DETECTION

#ifdef MEMORY_LEAKS_DETECTION
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#if HAVE_CUDA == 1

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <nvToolsExt.h>
#include <iomanip>
#include "cudadecoder/batched-threaded-nnet3-cuda-online-pipeline.h"
#include "cudadecoder/cuda-online-pipeline-dynamic-batcher.h"
#include "cudadecoderbin/cuda-bin-tools.h"
//#include "cudadecoderbin/custom_backend.h"
#include "cudadecoderbin/cuda-speaker-diarization-tools.h"
#include "cudamatrix/cu-allocator.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "nnet3/am-nnet-simple.h"
#include "nnet3/nnet-utils.h"
#include "util/kaldi-thread.h"
#include "lat/sausages.h"
#include "json.h"

#include <iostream>
#include <istream>
#include <streambuf>
#include <string>


//Web socket-related includes
#include "libwebsockets.h"
#include <signal.h>
#pragma comment(lib, "websockets.lib") // comment out when ubuntu
#pragma comment(lib, "libprotobuf.lib") // comment out when ubuntu
//#pragma comment(lib, "libprotobufd.lib") // comment out when ubuntu

#include "wavchunk.pb.h"
using namespace revcord;
using namespace std;


enum ErrorCodes {
    kSuccess,
    kUnknown,
    kInvalidModelConfig,
    kGpuNotSupported,
    kSequenceBatcher,
    kModelControl,
    kInputOutput,
    kInputName,
    kOutputName,
    kInputOutputDataType,
    kInputContents,
    kInputSize,
    kOutputBuffer,
    kBatchTooBig,
    kTimesteps,
    kChunkTooBig
};

using namespace std;
using namespace kaldi;
using namespace cuda_decoder;

CudaOnlineBinaryOptions opts_;
BatchedThreadedNnet3CudaOnlinePipeline* cuda_pipeline_ = NULL;

fst::SymbolTable* word_syms_;
std::unordered_set<BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID> is_corr_id_in_batch_;
std::vector<BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID> batch_corr_ids_;
std::vector<kaldi::SubVector<kaldi::BaseFloat>> batch_wave_samples_;
std::vector<bool> batch_is_last_chunk_;
std::vector<bool> batch_is_first_chunk_;

bool latest_sent_msg_was_buffered = false;
// Reply with ....
unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 65536 * 8 + LWS_SEND_BUFFER_POST_PADDING];
unsigned char* writable_buffer = &buf[LWS_SEND_BUFFER_PRE_PADDING];
size_t writable_size_ = 0;

BatchSTTChunk batch_stt_result_; // 
std::mutex batch_stt_result_m_;

// Contants 
const uint64_t int32_byte_size_ = 4;
const uint64_t int64_byte_size_ = 8;
const uint64_t chunk_num_bytes_ = 8160;
const uint64_t chunk_num_samps_ = 4080;// 8000 * 0.51

#ifdef XVECTOR_SD
Vector<BaseFloat> xvector_mean_;
Matrix<BaseFloat> xvector_transform_;
Plda plda_;
BatchedXvectorComputer* xvector_computer_;

std::string SpeakerDiarization(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat,
    const Matrix<BaseFloat>& h_utt_features,
    const BatchedXvectorComputer& xvector_computer,
    const Vector<BaseFloat>& xvector_mean,
    const Matrix<BaseFloat>& xvector_transform,
    const Plda& plda) {

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    for (int i = 0; i < size; i++) {
        json::JSON word;
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }

    std::vector<Vector<BaseFloat>> xvectors = xvector_computation(xvector_computer, h_utt_features, word_sequence);
    bool has_xvector = (xvectors.size() > 0);
    std::vector<int32> spk_ids;
    int num_speakers = VBDiarization(opts.batched_xvector_opts, xvector_mean, xvector_transform, plda, xvectors, spk_ids);
    std::string out_str = MakeFinalJSON(has_xvector, spk_ids, num_speakers, word_sequence);
    return out_str;
}
#endif

std::string BuildFinalResult(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat) 
{

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    json::JSON word;
    for (int i = 0; i < size; i++) {
        
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }
    std::string out_str = MakeFinalJSONOnlyForSTT(word_sequence);
    return out_str;
}



int FlushBatch() {
    if (!batch_corr_ids_.empty()) {
        cuda_pipeline_->DecodeBatch(batch_corr_ids_, batch_wave_samples_,
            batch_is_first_chunk_,
            batch_is_last_chunk_);
        is_corr_id_in_batch_.clear(); 
        batch_corr_ids_.clear();
        batch_is_first_chunk_.clear();
        batch_is_last_chunk_.clear();
        batch_wave_samples_.clear();
    }
    return 0;
}

int Execute(struct lws* wsi, BatchChunk batch) {
    
    is_corr_id_in_batch_.clear();
    batch_corr_ids_.clear();
    batch_is_first_chunk_.clear();
    batch_is_last_chunk_.clear();
    batch_wave_samples_.clear();
    if (Info_Kind::Info_Kind_REFRESH == batch.info().kind())
    {
        cuda_pipeline_->FreeChannelMap();
        BatchSTTChunk cmd_result;
        Info info;
        info.set_kind(Info_Kind::Info_Kind_EMPTY);
        cmd_result.set_allocated_info(&info);
        writable_size_ = cmd_result.ByteSizeLong();
        cmd_result.SerializeToArray(writable_buffer, writable_size_);
        lws_callback_on_writable(wsi);
        cout << std::endl << "Initialized the server " << std::endl;
        return kSuccess;
    }
    else if (Info_Kind::Info_Kind_GETSTT == batch.info().kind())
    {
        {
            std::lock_guard<std::mutex> lk(batch_stt_result_m_);
            if (batch_stt_result_.chunks_size() > 0)
            {
                Info info;
                info.set_kind(Info_Kind::Info_Kind_NORMAL);
                batch_stt_result_.set_allocated_info(&info);
                writable_size_ = batch_stt_result_.ByteSizeLong();
                batch_stt_result_.SerializeToArray(writable_buffer, writable_size_);
#ifdef lws_verbose 
                for (int i = 0; i < batch_stt_result_.chunks_size(); i++)
                {
                    if (batch_stt_result_.chunks().at(i).is_final_chunk())
                        //cout << "final result chunk for corr_id = " << batch_stt_result_.chunks().at(i).corr_id() << endl;
                        cout << "final result chunk for corr_id = " << batch_stt_result_.chunks().at(i).result() << endl;
                }

#endif
                batch_stt_result_.clear_chunks();
                
            }
            else
            {
                Info info;
                info.set_kind(Info_Kind::Info_Kind_EMPTY);
                BatchSTTChunk empty_result;
                empty_result.set_allocated_info(&info);
                writable_size_ = empty_result.ByteSizeLong();
                empty_result.SerializeToArray(writable_buffer, writable_size_);
            }
            lws_callback_on_writable(wsi);
        }
        return kSuccess;
    }
    else if (Info_Kind::Info_Kind_WAV == batch.info().kind())
    {
        int payload_cnt = batch.chunks_size();
        /*
        if (payload_cnt > opts_.num_streaming_channels)
        {
            cout << "Received wav batch size " << payload_cnt << " is bigger than num_streaming_channels = opts_.num_streaming_channels " << "Server exits now" << std::endl;
            return kBatchTooBig;
        }
        else
        {
            //cout << "Received wav batch size = " << payload_cnt << std::endl;
        }
        */
        // Each payload is a chunk for one sequence
        // Currently using dynamic batcher, not sequence batcher
        for (uint32_t pidx = 0; pidx < payload_cnt; ++pidx) {

            int error_code = kSuccess;
            WavChunk& chunk = (WavChunk&)batch.chunks(pidx);
            BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID corr_id = chunk.corr_id();


            bool corr_id_not_in_batch;
            decltype(is_corr_id_in_batch_.end()) is_corr_id_in_batch_it;
            std::tie(is_corr_id_in_batch_it, corr_id_not_in_batch) =
                is_corr_id_in_batch_.insert(corr_id);
            if (corr_id_not_in_batch) {
                bool start = chunk.is_first_chunk();
                bool end = chunk.is_last_chunk();
                if (start) {
                    if (!cuda_pipeline_->TryInitCorrID(corr_id, 0)) {
                        KALDI_LOG << "All decoding channels are in use. Consider "
                            "increasing --num-channels";
                        is_corr_id_in_batch_.erase(
                            is_corr_id_in_batch_it);  // this elt won't be in the batch
                        continue;// Ignoring this elt, we'll retry later
                    }
                }
                // first chunk with this corr_id in batch

                int32_t length = chunk.samples().length();
                //std::cout << "sample length = " << length << " for corr_id = " << corr_id << std::endl;
                Vector<BaseFloat> wave;
                wave.Resize(length / 2, kUndefined);
                for (int i = 0; i < length / 2; i++)
                    wave(i) = *(((short*)chunk.samples().data()) + i);
                SubVector<BaseFloat> wave_part(wave, 0, length / 2);
                // Add to batch
                batch_corr_ids_.push_back(corr_id);
                batch_wave_samples_.push_back(wave_part);
                batch_is_last_chunk_.push_back(end);
                batch_is_first_chunk_.push_back(start);
                
                if (start) {
                    // Defining the callback for the results
                    cuda_pipeline_->SetBestPathCallback(
                        corr_id,
                        [corr_id](
                            const std::string& str, bool partial, bool endpoint_detected) {
                                /*
                                STTChunk* chunk = NULL;
                                {
                                    std::lock_guard<std::mutex> lk(batch_stt_result_m_);
                                    chunk = batch_stt_result_.add_chunks();

                                    json::JSON res;
                                    if (endpoint_detected)
                                    {
                                        res["text"] = str;
                                        chunk->set_corr_id(corr_id);
                                        chunk->set_is_partial_chunk(false);
                                        chunk->set_is_result_chunk(true);
                                        chunk->set_is_final_chunk(false);
                                        chunk->set_result(res.dump().c_str());
                                    }
                                    else if (partial) {
                                        res["partial"] = str;
                                        chunk->set_corr_id(corr_id);
                                        chunk->set_is_partial_chunk(true);
                                        chunk->set_is_result_chunk(false);
                                        chunk->set_is_final_chunk(false);
                                        chunk->set_result(res.dump().c_str());
                                    }
                                }
                                */

                        });
                }
                if (end) {
                    // If last chunk, set the callback for that seq
                    cuda_pipeline_->SetLatticeCallback(
#ifdef XVECTOR_SD
                        corr_id, [corr_id](CompactLattice& clat, Matrix<BaseFloat>& h_utt_features) {
                            std::string output = SpeakerDiarization(opts_, *word_syms_, clat, h_utt_features, *xvector_computer_, xvector_mean_, xvector_transform_, plda_);
                            //std::string output = "{\"text\": \"\"}";
#else
                        corr_id, [corr_id](CompactLattice& clat) {
                            //std::string output = "{\"text\": \"\"}";
                            std::string output = BuildFinalResult(opts_, *word_syms_, clat);
#endif
                            /*
                            STTChunk* chunk = NULL;
                            {
                                std::lock_guard<std::mutex> lk(batch_stt_result_m_);
                                chunk = batch_stt_result_.add_chunks();
                                chunk->set_corr_id(corr_id);
                                chunk->set_result(output.c_str());
                                chunk->set_is_partial_chunk(false);
                                chunk->set_is_result_chunk(false);
                                chunk->set_is_final_chunk(true);
                            }
                            */
                        });
                }
                
                if (batch_corr_ids_.size() == opts_.batched_decoder_config.max_batch_size) FlushBatch();

                } // if(corr_id_not_in_batch)
            else
            {
                // Ignoring this element, we already have this corr_id in batch
                continue;
            }

            }
        FlushBatch();
        cuda_pipeline_->WaitForLatticeCallbacks();
        /*
        // Reply with empty STT result
        BatchSTTChunk cmd_result;
        Info info;
        info.set_kind(Info_Kind::Info_Kind_EMPTY);
        cmd_result.set_allocated_info(&info);
        writable_size_ = cmd_result.ByteSizeLong();
        cmd_result.SerializeToArray(writable_buffer, writable_size_);
        lws_callback_on_writable(wsi);
        */
        return kSuccess;
    }
    
    return -1;
    
}





//*****************************
//*** AUDIO STREAMING LAYER ***
//*****************************

//Server configuration definitions
#define TIME_INTERVAL 50
const int DEFAULT_SERVER_PORT = 2571;
static int interrupted, port = DEFAULT_SERVER_PORT;

// audio stream buffers
const int MAX_NUM_CHANNELS = 1024;
//relevant connection (a hack which can be only used in a single client mode)
struct lws* myWSI;

struct audio_streaming_protocol_instance {
    char buffer[(16 + chunk_num_bytes_) * MAX_NUM_CHANNELS];
    uint64_t total_bytes_received;
};

static int
callback_audio_streaming(struct lws* wsi,
    enum lws_callback_reasons reason,
    void* user, void* in, size_t len)
{

    struct audio_streaming_protocol_instance* ldi = (audio_streaming_protocol_instance*)user;
    
    switch (reason) {
    case LWS_CALLBACK_ESTABLISHED: {
        /*Check if another client is connected or not*/
        ldi->total_bytes_received = 0;
        writable_size_ = sprintf((char*)writable_buffer, "Connection Allowed");
        lws_callback_on_writable(wsi);
        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << "the last connection established at " << std::ctime(&end_time) << std::endl;
        break;
    }

    case LWS_CALLBACK_CLOSED: {
        //lwsl_user("LWS_CALLBACK_CLOSED....\n");
        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << "the last connection closed at " << std::ctime(&end_time) << std::endl;
        break;
    }

    case LWS_CALLBACK_RECEIVE:
        try {
            char* buffer = (char*)in;
            std::memcpy(ldi->buffer + ldi->total_bytes_received, buffer, len);
            ldi->total_bytes_received += len;
            if (lws_is_final_fragment(wsi) && !lws_remaining_packet_payload(wsi)) { //EOC event

                BatchChunk batch;
                size_t size = ldi->total_bytes_received;
                batch.ParseFromArray(ldi->buffer, size);      // convert to batch message 
                ldi->total_bytes_received = 0;
                if (Execute(wsi, batch) != kSuccess)
                    return -1;
                batch.Clear();
            }
            else
            {
               // std::cout << "fragment...." << std::endl;
            }
        }
        catch (const std::exception& e) {
            std::cerr << e.what();
        }
        break;
    case LWS_CALLBACK_SERVER_WRITEABLE: {
        
        if (latest_sent_msg_was_buffered) {
            if (lws_partial_buffered(wsi)) {
                latest_sent_msg_was_buffered = true;
                lws_callback_on_writable(wsi);
                cout << "latest_sent_msg_was_buffered in server " << endl;
                break;
            }
            else
            { 
                latest_sent_msg_was_buffered = false;
            }
            break;
        }
        

        int n = 0;
        n = lws_write(wsi, writable_buffer, writable_size_, LWS_WRITE_BINARY);
#ifdef lws_verbose
        
        if (n < 0)
            std::cout << "Error Sending for size = " << writable_size_ << std::endl;
        else if (n < writable_size_)
            std::cout << "Partial write" << std::endl;
        else if (n == writable_size_) 
            std::cout << "Write successful " << writable_size_ / 1024.0 <<"(KB)" << std::endl;
#endif
        if (lws_partial_buffered(wsi)) {
            latest_sent_msg_was_buffered = true;
            cout << "latest sent msg was_buffered in server " << n / 1024.0 << "(KB)" << std::endl;
            lws_callback_on_writable(wsi);
            break;
        }
        break;

    }

    default:
        break;
    }
    return 0;
}

static struct lws_protocols protocols[] = {
    // first protocol must always be HTTP handler //
    {
        "audio-streaming-protocol", // protocol name - very important!
        callback_audio_streaming,   // callback
        sizeof(struct audio_streaming_protocol_instance), // per_session_data_size
        65536 * 8, // rx_buffer_size
    0,    // id
    NULL, // user
    0     // tx_packet_size
    },
    {
        NULL, NULL, 0, 0   /* End of list */
    }
};

void sigint_handler(int sig)
{
    interrupted = 1;
}

/***************************
*   Logging
*
***************************/
LWS_VISIBLE LWS_EXTERN void lwsl_emit_stderr(int level,
    const char* line
)
{
    std::cout << "LWS_LOG():" << line << std::endl;
}
//******************
//*** EXECUTABLE ***
//******************


int main(int argc, char* argv[]) {
    try {

#ifdef MEMORY_LEAKS_DETECTION
        _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

        // Verify that the version of the library that we linked against is
        // compatible with the version of the headers we compiled against.
        GOOGLE_PROTOBUF_VERIFY_VERSION;

        // KALDI INITIALIZATION START

        using namespace kaldi;
        using namespace fst;

        typedef kaldi::int32 int32;
        typedef kaldi::int64 int64;



        int errno;
        if ((errno = SetUpAndReadCmdLineOptions(argc, argv, &opts_))) return errno;

        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        nnet3::Nnet xvector_nnet;
        fst::Fst<fst::StdArc>* decode_fst;
#ifdef XVECTOR_SD 
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_,
            &xvector_nnet, &xvector_mean_, &xvector_transform_, &plda_, &xvector_computer_);
#else
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_);
#endif
        cuda_pipeline_ = new BatchedThreadedNnet3CudaOnlinePipeline(
            opts_.batched_decoder_config, *decode_fst, am_nnet, trans_model);
        delete decode_fst;
        if (word_syms_) cuda_pipeline_->SetSymbolTable(*word_syms_);

        nvtxRangePush(L"Global Timer");

        // KALDI INITIALIZATION FINISHED

        // COMMUNICATION LAYER INITIALIZATION

        struct lws_context_creation_info info;
        struct lws_context* context;
        int n = 0, logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE
            /* for LLL_ verbosity above NOTICE to be built into lws,
            * lws must have been configured and built with
            * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
            /* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
            /* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
            /* | LLL_DEBUG */;

        signal(SIGINT, sigint_handler);
        lws_set_log_level(logs, lwsl_emit_stderr);
        //lws_set_log_level(logs, NULL);
        lwsl_user("REVCORD LWS server\n");

        memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
        info.port = port;
        info.protocols = protocols;
        info.pt_serv_buf_size = 1024 * 1024 * 8; // 8M
        info.options = LWS_SERVER_OPTION_VALIDATE_UTF8 |
            LWS_SERVER_OPTION_HTTP_HEADERS_SECURITY_BEST_PRACTICES_ENFORCE;
        
        /*
        info.ka_time = 3600;//3600*24
        info.ka_interval = 60;
        info.ka_probes = 10;
        */
                
        context = lws_create_context(&info);
        if (context == NULL) {
            KALDI_LOG << "libwebsocket init failed";
            return -1;
        }

        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        std::cout << "Server started at " << std::ctime(&end_time) << std::endl;

        // infinite loop, to end this server send SIGTERM. (CTRL+C)
        while (n >= 0 && !interrupted)
            n = lws_service(context, 0);
        lws_context_destroy(context);
        lwsl_user("Completed %s\n", interrupted == 2 ? "OK" : "failed");

        nvtxRangePop();
        delete word_syms_;  // will delete if non-NULL.
        delete cuda_pipeline_;
        cudaDeviceSynchronize();

        // Optional:  Delete all global objects allocated by libprotobuf.
        google::protobuf::ShutdownProtobufLibrary();

#ifdef MEMORY_LEAKS_DETECTION
        _CrtDumpMemoryLeaks();
#endif

        return interrupted != 2;
    }
    catch (const std::exception& e) {
        std::cerr << e.what();
        return -1;
    }
}  // main()

#endif  // if HAVE_CUDA == 1
