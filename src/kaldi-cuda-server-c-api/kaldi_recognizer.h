// kaldi_recognizser.h
//
//  
//  Created by Jin Zhe Feng on 15/5/20.
//  Copyright � 2020 Revcord, Inc. All rights reserved.
//

#ifndef kaldi_recognizer_H_
#define kaldi_recognizer_H_

#include "model.h"
#include "icallbackhandler.h"

using namespace kaldi;



class KaldiRecognizer {
    public:
        KaldiRecognizer(Model* model, float sample_rate);
        KaldiRecognizer() {};
        ~KaldiRecognizer();
		void SetVerboseLevel(int log_level);
        bool AcceptWaveform(const char* data, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk);
        bool AcceptWaveform(const short* sdata, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk);
        bool AcceptWaveform(const float* fdata, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk);
        bool AcceptWaveform(SubVector<BaseFloat> wave_part, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk);
        void Initialize();
        const char* Result(); 
        const char* FinalResult();
        const char* PartialResult(); 
        string SpeakerDiarization(const kaldi::CompactLattice& clat, const Matrix<BaseFloat>& h_utt_features);
        uint64_t GetCorrID();
        double GetLatency() { return latency; }
        double TimerElapsed();
        void microSleep(uint64_t usec);
        void SetupCallback(ICallbackHandler* handler);
        // CallbackHandler for C# linkage
        ICallbackHandler* CallbackHandler;
    private:

        BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID corr_id;
        kaldi::cuda_decoder::BatchedThreadedNnet3CudaOnlinePipeline* cuda_pipeline;
        CudaOnlinePipelineDynamicBatcher* dynamic_batcher;
        fst::SymbolTable* word_syms;
        Vector<BaseFloat> *xvector_mean;
        Matrix<BaseFloat>* xvector_transform;
        Plda* plda;
        BatchedXvectorComputer* xvector_computer;
        BatchedXvectorComputerOptions batched_xvector_opts;
        double latency;
        bool is_first_chunk;
        bool is_last_chunk;
        float sample_rate_ = 8000;
        Model *stt_model_;

        string partial_result;
        string endpoint_result;
        string final_result;
};
#endif /* kaldi_recognizer_H_ */