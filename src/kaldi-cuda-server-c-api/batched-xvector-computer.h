// batched-xvector-computer.h
//
//  
//  Created by Jin Zhe Feng on 10/2/21.
//  Copyright © 2021 Revcord, Inc. All rights reserved.
//


#ifndef BATCHED_XVECTOR_COMPUTER_H_
#define BATCHED_XVECTOR_COMPUTER_H_

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <nvToolsExt.h>
#include <iomanip>
#include <sstream>
#include "cudadecoder/batched-threaded-nnet3-cuda-online-pipeline.h"
#include "cudamatrix/cu-allocator.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "nnet3/am-nnet-simple.h"
#include "nnet3/nnet-utils.h"
#include "util/kaldi-thread.h"
#include "lat/sausages.h"


using namespace kaldi;
using namespace std;
using namespace nnet3;

struct BatchedXvectorComputerOptions {
    int32 chunk_size{ 150 };
    int32 batch_size{ 32 };
    bool pad_input{ true };
    float target_energy{ 0.1 };
    int32 num_speakers{ -1 };
    NnetComputeOptions compute_config;
    NnetOptimizeOptions optimize_config;
    CachingOptimizingCompilerOptions compiler_config;


    void Register(OptionsItf* po) {
        po->Register("chunk-size", &chunk_size,
            "Size of chunk, in input frames.  Includes the nnet "
            "context, so the number of chunks will be more than "
            "total-input-frames / chunk-size.");
        po->Register("target-energy", &target_energy,
            "target engery for PCA analysis.");
        po->Register("num-speakers", &num_speakers,
            "known number of speakers for speaker diarization.");
        po->Register("xvector-batch-size", &batch_size,
            "Size of the batches of chunks that we compute at once. ");
        po->Register("pad-input", &pad_input,
            "If true, for utterances shorter than `chunk-size` frames "
            "we will pad with repeats of the last frame.");
        compute_config.Register(po);
        optimize_config.Register(po);
        compiler_config.Register(po);
    }
};

class BatchedXvectorComputer {
public:
    /**
            @param [in]  opts  Options class; warning, it keeps a reference to it.
            @param [in]  nnet  The neural net we'll be computing with; assumed to have
                            already been prepared for test.
            @param [in] total_context   The sum of the left and right context of the
                            network, computed after calling
                            SetRequireDirectInput(true, &nnet); so the l/r context
                            isn't zero.
        */

    BatchedXvectorComputer(const BatchedXvectorComputerOptions& opts,
        const Nnet& nnet,
        int32 total_context);

    /**
        Accepts an utterance to process into an xvector, and, if one or more
        batches become full, processes the batch.
        */
    void AcceptUtterance(const std::string& utt,
        const Matrix<BaseFloat>& input);


    /**  Returns true if at least one xvector is pending output (i.e. that
            the user may call OutputXvector()).
        */
    bool XvectorReady() const;

    /**
        This function, which must only be called if XvectorReady() has
        just returned true,  outputs an xvector for an utterance.
            @param [out] utt  The utterance-id is written to here.
                            Note: these will be output in the same order
                            as the user called AcceptUtterance(), except
                            that if opts_.pad_input is false and
                            and utterance is shorter than the chunk
                            size, some utterances may be skipped.
            @param [out] xvector  The xvector will be written to here.
        */
    void OutputXvector(std::string* utt,
        Vector<BaseFloat>* xvector);


    /**
        Calling this will force any partial minibatch to be computed,
        so that any utterances that have previously been passed to
        AcceptUtterance() will, when this function returns, have
        their xvectors ready to be retrieved by OutputXvector().
        */
    void Flush();


private:

    struct XvectorTask {
        std::string utt_id;
        int32 num_chunks;
        int32 num_chunks_finished;
        Vector<BaseFloat> xvector;
        XvectorTask* tail;
    };


    /**
        This decides how to split the utterance into chunks.  It does so in a way
        that minimizes the variance of the x-vector under some simplifying
        assumptions.  It's about minimizing the variance of the x-vector.  We treat
        the x-vector as computed as a sum over frames (although some frames may be
        repeated or omitted due to gaps between chunks or overlaps between chunks);
        and we try to minimize the variance of the x-vector estimate; this is minimized
        when all the frames have the same weight, which is only possible if it can be
        exactly divided into chunks; anyway, this function computes the best division
        into chunks.

        It's a question of whether to allow overlaps or gaps.
        Suppose we are averaging independent quantities with variance 1.  The
        variance of a simple sum of M of those quantities is 1/M.
        Suppose we have M of those quantities, plus N which are repeated twice
        in the sum.  The variance of the estimate formed that way is:

        (M + 4N) / (M + 2N)^2

        If we can't divide it exactly into chunks we'll compare the variances from
        the cases where there is a gap vs. an overlap, and choose the one with
        the smallest variance.  (Note: due to context effects we actually lose
        total_context_ frames from the input signal, and the chunks would have
        to overlap by total_context_ even if the part at the statistics-computation
        layer were ideally cut up.

            @param [in] num_frames  The number of frames in the utterance
            @param [out] start_frames  This function will output to here a vector
                        containing all the start-frames of chunks in this utterance.
                        All chunks will have duration opts_.chunk_size; if a chunk
                        goes past the end of the input we'll repeat the last frame.
                        (This will only happen if opts_.pad_input is false and
                        num_frames is less than opts_.chunk_length.)
        */
    void SplitUtteranceIntoChunks(int32 num_frames,
        std::vector<int32>* start_frames);

    /** This adds a newly created XvectorTask at the tail of the singly linked
        list whose (head,tail) are results_head_, results_tail_.
        */
    XvectorTask* CreateTask(const std::string& utt, int32 num_chunks);


    /**
        Does the nnet computation for one batch and distributes the
        computed x-vectors (of chunks) appropriately to their XvectorTask
        objects.
        */
    void ComputeOneBatch();

    /**
        Adds a new chunk to a batch we are preparing.  This will go
        at position `position_in_batch_` which will be incremented.
            @param [in] task  The task this is part of (records the
                    utterance); tasks_this_batch_[position_in_batch_] will
                    be set to this.
            @param [in] input  The input matrix of features of
                    which this chunk is a part
            @param [in] chunk_start  The frame at which this
                    chunk starts.  Must be >= 0; and if
                    opts_.pad_input is false, chunk_start + opts_.chunk_size
                    must be <= input.NumRows().
        */
    void AddChunkToBatch(XvectorTask* task,
        const Matrix<BaseFloat>& input,
        int32 chunk_start);

    const BatchedXvectorComputerOptions& opts_;
    int32 total_context_;
    const Nnet& nnet_;

    int32 feature_dim_;
    int32 xvector_dim_;

    /**
        Staging area for the input features prior to copying them to GPU.
        Dimension is opts_.chunk_size * opts_.batch_size by feature_dim_.  The
        sequences are interleaved (will be faster since this corresponds to how
        nnet3 keeps things in memory), i.e. row 0 of input_feats_ is time t=0
        for chunk n=0; and row 1 of input_feats_ is time t=0 for chunk n=1.
    */
    Matrix<BaseFloat> input_feats_;


    /** The compiled computation (will be the same for every batch).  */
    std::shared_ptr<const NnetComputation> computation_;


    /**  position_in_batch_ is the number of chunks that we have filled in in
            the input_feats_ matrix and tasks_this_batch_.  When it reaches
            opts_.batch_size we will do the actual computation.
    */
    int32 position_in_batch_;

    /**
        tasks_this_batch_ is of dimension opts_.batch_size.  It is a vector of pointers to
        elements of the singly linked list whose head is at results_head_, or
        NULL for elements with indexes >= position_in_batch_.
        */
    std::vector<XvectorTask*> tasks_this_batch_;

    // results_head_ is the first element in the singly linked list of
    // already-computed xvectors, or NULL if that list is empty.  Note:
    // utterances that are ready will appear here first; new utterances
    // get added to the tail.
    XvectorTask* results_head_;
    // results_tail_ is the last element in the singly linked list of
    // already-computed xvectors, or NULL if the list is empty.
    XvectorTask* results_tail_;
};


#endif /* BATCHED_XVECTOR_COMPUTER_H_ */