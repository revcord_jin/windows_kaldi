#ifndef ICALLBACKHANDLER_H_
#define ICALLBACKHANDLER_H_


class ICallbackHandler
{
public:
    virtual ~ICallbackHandler() {}
    virtual void latticehandle(const int corr_id, const char *str) = 0;
    virtual void bestpathhandle(const int corr_id, const char *str, const bool endpoint_detected) = 0;
};
#endif /* ICALLBACKHANDLER_H_ */	