%module (directors="1") revcordstt

%include <typemaps.i>
%include <windows.i>
%include <arrays_csharp.i>

CSHARP_ARRAYS(char, byte)
%apply char INPUT[] { const char *data , const char *grammar };
%apply float INPUT[] {const float *fdata};
%apply short INPUT[] {const short *sdata};

CSHARP_ARRAYS(char *, string)
%apply char *INPUT[]  { char *argv[] }

%{
#include "revcord_api.h"
typedef struct RevcordSTTModel Model;
typedef struct RevcordRecognizer KaldiRecognizer;
%}

typedef struct {} Model;
typedef struct {} KaldiRecognizer;
typedef struct {} ICallbackHandler;

// generate directors for all classes that have virtual methods
%feature("director") ICallbackHandler; 
%include "icallbackhandler.h"

%extend Model {
    Model(int argc, char* argv[]){
        return revcord_stt_model_new(argc, argv);
    }
    ~Model() {
        revcord_stt_model_free($self);
    }
}

%extend KaldiRecognizer {
    KaldiRecognizer(Model *model, float sample_rate)  {
        return revcord_recognizer_new(model, sample_rate);
    }
    KaldiRecognizer(Model *model, float sample_rate, const char* grammar)  {
        return revcord_recognizer_new(model, sample_rate);
    }
}



