// model.cc
//
//  
//  Created by Jin Zhe Feng on 15/5/20.
//  Copyright � 2020 Revcord, Inc. All rights reserved.
//

//
// Possible model layout:
//
// * Default kaldi model with HCLG.fst
//
// * Lookahead model with const G.fst
//
// * Lookahead model with ngram G.fst
//
// * File disambig_tid.int required only for lookadhead models
//
// * File word_boundary.int is required if we want to have precise word timing information
//   otherwise we don't do any word alignment. Optionally lexicon alignment can be done
//   with corresponding C++ code inside kaldi recognizer.


#include "model.h"

typedef kaldi::int32 int32;
typedef kaldi::int64 int64;

Model::Model(int argc, char** argv) {

    int errno;
    if ((errno = SetUpAndReadCmdLineOptions(argc, argv)))
        KALDI_ERR << "Error in options";
    // read transition model and nnet
    bool binary;
    Input ki(nnet3_rxfilename, &binary);
    trans_model.Read(ki.Stream(), binary);
    am_nnet.Read(ki.Stream(), binary);
    ki.Close();
    SetBatchnormTestMode(true, &(am_nnet.GetNnet()));
    SetDropoutTestMode(true, &(am_nnet.GetNnet()));
    CollapseModel(CollapseModelConfig(), &(am_nnet.GetNnet()));

    
    KALDI_LOG << " Loading large HCLG.fst ...";
    Fst<StdArc>* decode_fst = ReadFstKaldiGeneric(fst_rxfilename);
    KALDI_LOG << "done\n";
    
    KALDI_LOG << " Creating cuda_pipeline ...";
    cuda_pipeline = new BatchedThreadedNnet3CudaOnlinePipeline(
            batched_decoder_config, *decode_fst, am_nnet, trans_model);
    KALDI_LOG << "Creating cuda_pipeline ...Done\n";
        
    delete decode_fst;
    

    word_syms = nullptr;

    if (!word_syms_rxfilename.empty()) {
        if (!(word_syms = SymbolTable::ReadText(word_syms_rxfilename)))
            KALDI_ERR << "Could not read symbol "
            "table from file "
            << word_syms_rxfilename;
    }
    if (word_syms) cuda_pipeline->SetSymbolTable(*word_syms);

    chunk_length = cuda_pipeline->GetNSampsPerChunk();
    chunk_seconds = cuda_pipeline->GetSecondsPerChunk();
    seconds_per_sample = chunk_seconds / chunk_length;
    
    CudaOnlinePipelineDynamicBatcherConfig dynamic_batcher_config;
    dynamic_batcher = new CudaOnlinePipelineDynamicBatcher(dynamic_batcher_config, *cuda_pipeline);

    
    string xvector_extractor_path = sd_model_path_rxfilename + "/final.extractor.raw";
    ReadKaldiObject(xvector_extractor_path, &xvector_nnet);
    SetBatchnormTestMode(true, &xvector_nnet);
    SetDropoutTestMode(true, &xvector_nnet);
    CollapseModel(CollapseModelConfig(), &xvector_nnet);
    KALDI_ASSERT(IsSimpleNnet(xvector_nnet));
    
    int total_context;
    {
        int32 left_context, right_context;
        // Compute left_context, right_context as the 'real' left/right context
        // of the network; they'll tell us how many frames on the chunk boundaries
        // won't really participate in the statistics averaging.
        // SetRequireDirectInput()  modifies how the StatisticsPoolingComponent
        // treats its dependences, so we'll get the 'real' left/right context.
        SetRequireDirectInput(true, &xvector_nnet);
        ComputeSimpleNnetContext(xvector_nnet, &left_context, &right_context);
        KALDI_LOG << "Left/right context is " << left_context << ", "
            << right_context;
        SetRequireDirectInput(false, &xvector_nnet);
        total_context = left_context + right_context;
    }
    xvector_computer = new BatchedXvectorComputer(batched_xvector_opts, xvector_nnet, total_context);
    
    xvector_mean = new Vector<BaseFloat>();
    std::string mean_rxfilename = sd_model_path_rxfilename + "/plda/mean.vec";
    ReadKaldiObject(mean_rxfilename, xvector_mean);

    xvector_transform = new Matrix<BaseFloat>();
    std::string transform_rxfilename = sd_model_path_rxfilename + "/plda/transform.mat";
    ReadKaldiObject(transform_rxfilename, xvector_transform);
    plda = new Plda();
    std::string plda_rxfilename = sd_model_path_rxfilename + "/plda/plda";
    ReadKaldiObject(plda_rxfilename, plda);
    ref_cnt_ ++;
    KALDI_LOG << "-------------  End of Model Initialization ---------------";
}

Model::~Model() {
    delete cuda_pipeline;
    delete dynamic_batcher;
    delete xvector_computer;
    delete xvector_mean;
    delete xvector_transform;
    delete plda;
}

void Model::Ref()
{
    ref_cnt_++;
}

void Model::Unref()
{
    ref_cnt_--;
    if (ref_cnt_ == 0) {
        delete this;
    }
}

void Model::WaitForCompletion()
{
    dynamic_batcher->WaitForCompletion();
}

int Model::SetUpAndReadCmdLineOptions(int argc, char** argv) {

    for (int i = 0; i < argc; i++)
    {
        KALDI_LOG << argv[i];
    }

    const char* usage =
        "Usage: ";

    ParseOptions po(usage);
    po.Register("word-symbol-table", &word_syms_rxfilename,
        "Symbol table for words [for debug output]");
    po.Register("num-parallel-streaming-channels", &num_streaming_channels,
        "Number of channels streaming in parallel");

    
    CuDevice::RegisterDeviceOptions(&po);
    RegisterCuAllocatorOptions(&po);
    batched_decoder_config.Register(&po);
    batched_xvector_opts.Register(&po);
    po.Read(argc, argv);
    
    if (po.NumArgs() != 3) {
        po.PrintUsage();
        return 1;
    }
    g_cuda_allocator.SetOptions(g_allocator_options);
    CuDevice::Instantiate().SelectGpuId("yes");
    CuDevice::Instantiate().AllowMultithreading();

    batched_decoder_config.num_channels =
        std::max(batched_decoder_config.num_channels,
            2 * num_streaming_channels);

    nnet3_rxfilename = po.GetArg(1);
    fst_rxfilename = po.GetArg(2);
    sd_model_path_rxfilename = batched_decoder_config.xvector_model_path_rxfilename = po.GetArg(3);
    batched_decoder_config.xvector_feature_opts.mfcc_config = sd_model_path_rxfilename + "/mfcc.conf";// feature config for xvector 
    return 0;
}


