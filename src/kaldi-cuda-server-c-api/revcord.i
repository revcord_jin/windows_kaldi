%module (directors="1") revcordstt

%include <typemaps.i>
%include <windows.i>
%include <stdint.i>
%include <arrays_csharp.i>

CSHARP_ARRAYS(char, byte)
%apply unsigned char INPUT[] {const unsigned char *data};
%apply float INPUT[] {const float *fdata};
%apply short INPUT[] {const short *sdata};

CSHARP_ARRAYS(char *, string)
%apply char *INPUT[]  { char **argv }

%{
#include "revcord_api.h"
#include "icallbackhandler.h"
typedef struct RevcordSTTModel STTModel;
typedef struct RevcordRecognizer KaldiRecognizer;
%}


// generate directors for all classes that have virtual methods
%feature("director") ICallbackHandler; 
%include "icallbackhandler.h"

typedef struct {} STTModel;
typedef struct {} KaldiRecognizer;


%extend STTModel {
    STTModel(int argc, char** argv){
        return revcord_stt_model_new(argc, argv);
    }
    ~STTModel() {
        revcord_stt_model_free($self);
    }
    void WaitForCompletion(STTModel* model){
		revcord_stt_model_waitforcompletion(model);
	}
	int GetNumStreamingChannels(STTModel* model){
		return revcord_stt_num_streaming_channels(model);
	}
	double GetChunkSeconds(STTModel* model){
		return revcord_stt_get_chunk_seconds(model);
	}
}



%extend KaldiRecognizer {
    KaldiRecognizer(STTModel *stt_model, float sample_rate)  {
        return revcord_recognizer_new(stt_model, sample_rate);
    }
    KaldiRecognizer(STTModel *stt_model, float sample_rate, const char* grammar)  {
        return revcord_recognizer_new_grm(stt_model, sample_rate, grammar);
    }
    ~KaldiRecognizer() {
        revcord_recognizer_free($self);
    }

    bool AcceptWaveform(const unsigned char *data, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk) {
        return revcord_recognizer_accept_waveform($self, data, len, corr_id, is_first_chunk, is_last_chunk);
    }
    bool AcceptWaveform(const short *sdata, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk) {
        return revcord_recognizer_accept_waveform_s($self, sdata, len, corr_id, is_first_chunk, is_last_chunk);
    }
    bool AcceptWaveform(const float *fdata, int len, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk) {
        return revcord_recognizer_accept_waveform_f($self, fdata, len, corr_id, is_first_chunk, is_last_chunk);
    }

    const char* Result() {
        return revcord_recognizer_result($self);
    }
    const char* PartialResult() {
        return revcord_recognizer_partial_result($self);
    }
    const char* FinalResult() {
        return revcord_recognizer_final_result($self);
    }
	void SetLogLevel(int log_level) {
		return revcord_recognizer_set_log_level($self,log_level);
	}
	void Initialize() {
		return revcord_recognizer_initialize($self);
	}
	void SetupCallback(ICallbackHandler *callback) {
		return revcord_recognizer_setupcallback($self, callback);
	}
	uint64_t GetCorrID(){
		return revcord_recognizer_get_corr_id($self);
	}
	double Elapsed(){
		return revcord_recognizer_timer_elapsed($self);
	}
}



