// Copyright 2020 Alpha Cephei Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "revcord_api.h"
#include "kaldi_recognizer.h"
#include <string.h>

REVCORD_API RevcordSTTModel* revcord_stt_model_new(int argc, char** argv)
{
    return (RevcordSTTModel*)new Model(argc, argv);
}

REVCORD_API void revcord_stt_model_free(RevcordSTTModel* model)
{
    ((Model*)model)->Unref();
}

REVCORD_API void revcord_stt_model_waitforcompletion(RevcordSTTModel* model)
{
    ((Model*)model)->WaitForCompletion();
}

REVCORD_API int revcord_stt_num_streaming_channels(RevcordSTTModel* model)
{
    return ((Model*)model)->GetNumStreamingChannels();
}

REVCORD_API int revcord_stt_get_chunk_length(RevcordSTTModel* model)
{
    return ((Model*)model)->GetChunkLength();
}

REVCORD_API double revcord_stt_get_chunk_seconds(RevcordSTTModel* model)
{
    return ((Model*)model)->GetChunkSeconds();
}


REVCORD_API RevcordRecognizer* revcord_recognizer_new(RevcordSTTModel* stt_model, float sample_rate)
{
    return (RevcordRecognizer*)new KaldiRecognizer((Model*)stt_model, sample_rate);
}


REVCORD_API RevcordRecognizer* revcord_recognizer_new_grm(RevcordSTTModel* stt_model, float sample_rate, const char* grammar)
{
    return (RevcordRecognizer*)new KaldiRecognizer((Model*)stt_model, sample_rate);
}

REVCORD_API void revcord_recognizer_initialize(RevcordRecognizer* recognizer)
{
    ((KaldiRecognizer*)recognizer)->Initialize();
}

REVCORD_API void revcord_recognizer_setupcallback(RevcordRecognizer* recognizer, ICallbackHandler* callback)
{
    ((KaldiRecognizer*)recognizer)->SetupCallback(callback);
}

REVCORD_API int revcord_recognizer_accept_waveform(RevcordRecognizer* recognizer, const char* data, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    return ((KaldiRecognizer*)recognizer)->AcceptWaveform(data, length, corr_id, is_first_chunk, is_last_chunk);
}

REVCORD_API int revcord_recognizer_accept_waveform_s(RevcordRecognizer* recognizer, const short* sdata, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    return ((KaldiRecognizer*)recognizer)->AcceptWaveform(sdata, length, corr_id, is_first_chunk, is_last_chunk);
}

REVCORD_API int revcord_recognizer_accept_waveform_f(RevcordRecognizer* recognizer, const float* fdata, int length, uint64_t corr_id, bool is_first_chunk, bool is_last_chunk)
{
    return ((KaldiRecognizer*)recognizer)->AcceptWaveform(fdata, length, corr_id, is_first_chunk, is_last_chunk);
}

REVCORD_API const char* revcord_recognizer_result(RevcordRecognizer* recognizer)
{
    return ((KaldiRecognizer*)recognizer)->Result();
}

REVCORD_API const char* revcord_recognizer_partial_result(RevcordRecognizer* recognizer)
{
    return ((KaldiRecognizer*)recognizer)->PartialResult();
}

REVCORD_API const char* revcord_recognizer_final_result(RevcordRecognizer* recognizer)
{
    return ((KaldiRecognizer*)recognizer)->FinalResult();
}

REVCORD_API void revcord_recognizer_free(RevcordRecognizer* recognizer)
{
    delete ((KaldiRecognizer*)recognizer);
}

REVCORD_API void revcord_recognizer_set_log_level(RevcordRecognizer* recognizer, int log_level)
{
    ((KaldiRecognizer*)recognizer)->SetVerboseLevel(log_level);
}
REVCORD_API uint64_t revcord_recognizer_get_corr_id(RevcordRecognizer* recognizer)
{
    return ((KaldiRecognizer*)recognizer)->GetCorrID();
}
REVCORD_API double revcord_recognizer_timer_elapsed(RevcordRecognizer* recognizer)
{
    return ((KaldiRecognizer*)recognizer)->TimerElapsed();
}
REVCORD_API void revcord_recognizer_usleep(RevcordRecognizer* recognizer, uint64_t usec)
{
    ((KaldiRecognizer*)recognizer)->microSleep(usec);
}

